from django.apps import AppConfig


class StatusLoggerConfig(AppConfig):
    name = 'status_logger'
