from django.conf import settings
from rest_framework import viewsets, mixins
from rest_framework.permissions import AllowAny
from rest_framework.decorators import action
from django.shortcuts import get_object_or_404

from config.helper import MyResponse, code_generator

from status_logger.models import AppVersion
from user import permissions
from status_logger.serializers import AppVersionSerializer, CheckAppVersionSerializer
from django.contrib.auth import get_user


class AppVersionViewSet(viewsets.ModelViewSet):
    queryset = AppVersion.objects.all()
    serializer_class = AppVersionSerializer


    def get_permissions(self):
        permission_classes = []
        if self.action == 'partial_update' or self.action == 'destroy' or self.action == 'create' or self.action == 'update'\
                or self.action == 'list' or self.action == 'retrieve':
            permission_classes = [permissions.AdminOnly]
        elif self.action == 'checkversion':
            permission_classes = [AllowAny]
        return [permission() for permission in permission_classes]

    def create(self, request):
        user = request.user
        version = request.data['version']
        app_version = AppVersion.objects.filter(version=version, status=AppVersion.ACTIVE)

        if app_version:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "Version was created", '')

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by_id=user.id,
                        modified_by_id=user.id)

        data = {"detail": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                          "Version successfully created.", data)

    def list(self, request):
        queryset = self.queryset

        filter_version = request.GET.get('version', None)
        if filter_version:
            queryset = queryset.filter(version=str(filter_version))

        filter_status = request.GET.get('status', None)
        if filter_status:
            queryset = queryset.filter(status=str(filter_status))

        queryset = queryset.order_by('-created_date')

        serializer = self.get_serializer(queryset, many=True)
        data = {"list": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"], "",
                          data)

    def update(self, request, pk=None):
        user = request.user
        queryset = self.get_queryset()
        detail = get_object_or_404(queryset, pk=pk)

        if "version" not in request.data:
            request.data['version'] = str(detail.version)

        if "status" not in request.data:
            request.data['status'] = str(detail.status)


        # if another version already active
        another_version = AppVersion.objects.filter(version=detail.version, status=AppVersion.ACTIVE).exclude(id=detail.id)


        if another_version and request.data['status'] == 'ACTIVE':
            return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"],
                              settings.RESPONSE_META["SUCCESS"]["STATUS"],
                              "There's already a version that is active with the same version", '')

        serializer = self.get_serializer(detail, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(modified_by_id=user.id)

        data = {"detail": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                          "Version successfully updated.", data)

    @action(detail=False)
    def checkversion(self, request):

        queryset = self.get_queryset()
        queryset = queryset.filter(status=AppVersion.ACTIVE).order_by('created_date').last()
        if not queryset:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "No Version Found", '')

        current_version = request.GET.get('version', None)

        if not current_version:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "No Version Found", '')

        latest = True if str(queryset.version) == current_version else False

        serializer = CheckAppVersionSerializer(queryset, context={'latest': latest})
        data = {"detail": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                          "", data)