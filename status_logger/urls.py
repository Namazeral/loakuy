from django.urls import path, include
from rest_framework import routers
from status_logger import views, models


router = routers.DefaultRouter()
router.register(r'appversion', views.AppVersionViewSet)

urlpatterns = [
    path('', include(router.urls)),
]