from django.db import models

from transaction.models import Transaction
# Create your models here.

class StatusLogger(models.Model):
    class Meta:
        db_table = "status_logger"

    name = models.CharField(max_length=100, blank=True, null=True)
    transaction_id = models.ForeignKey(Transaction, blank=True, null=True, db_column='transaction_id', related_name='%(class)s_transaction_id', on_delete=models.PROTECT)
    created_by = models.ForeignKey("user.User", blank=True, null=True, db_column='created_by', related_name='%(class)s_created_by', on_delete=models.PROTECT)
    created_date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    modified_by = models.ForeignKey("user.User", blank=True, null=True, db_column='modified_by', related_name='%(class)s_modified_by', on_delete=models.PROTECT)
    modified_date = models.DateTimeField(auto_now=True, blank=True, null=True)

class AppVersion(models.Model):
    class Meta:
        db_table = "app_version"

    ACTIVE = 'ACTIVE'
    NOT_ACTIVE = 'NOT_ACTIVE'
    STATUS_CHOICES = (
        (ACTIVE, 'Active'),
        (NOT_ACTIVE, 'Not Active'),
    )

    version = models.CharField(max_length=50)
    required = models.BooleanField(default=True)
    description = models.TextField(blank=True, null=True)
    status = models.CharField(max_length=50, choices=STATUS_CHOICES, default=ACTIVE)
    created_by = models.ForeignKey("user.User", blank=True, null=True, db_column='created_by', related_name='%(class)s_created_by', on_delete=models.PROTECT)
    created_date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    modified_by = models.ForeignKey("user.User", blank=True, null=True, db_column='modified_by', related_name='%(class)s_modified_by', on_delete=models.PROTECT)
    modified_date = models.DateTimeField(auto_now=True, blank=True, null=True)