from rest_framework import serializers
from status_logger.models import AppVersion

class AppVersionSerializer(serializers.ModelSerializer):
    class Meta:
        model = AppVersion
        fields = '__all__'
        extra_kwargs = {'id': {'read_only': True}}

    def update(self, instance, validated_data):
        instance.version = validated_data.get('version', instance.version)
        instance.required = validated_data.get('required', instance.required)
        instance.description = validated_data.get('description', instance.description)
        instance.status = validated_data.get('status', instance.status)
        instance.save()

        return instance

class CheckAppVersionSerializer(serializers.ModelSerializer):
    latest = serializers.SerializerMethodField()

    class Meta:
        model = AppVersion
        fields = ('id','version', 'latest','required')
        extra_kwargs = {'id': {'read_only': True}}

    def get_latest(self, obj):
        return self.context.get("latest")