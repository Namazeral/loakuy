# Generated by Django 2.2.6 on 2020-01-27 06:35

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('transaction', '0007_auto_20191108_1454'),
    ]

    operations = [
        migrations.CreateModel(
            name='Profit',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nett_weight', models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=15, null=True)),
                ('unit', models.CharField(choices=[('KG', 'Kg'), ('TON', 'TON')], default='KG', max_length=50)),
                ('profit', models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=15, null=True)),
                ('created_date', models.DateTimeField(auto_now_add=True, null=True)),
                ('modified_date', models.DateTimeField(auto_now=True, null=True)),
                ('created_by', models.ForeignKey(blank=True, db_column='created_by', null=True, on_delete=django.db.models.deletion.PROTECT, related_name='profit_created_by', to=settings.AUTH_USER_MODEL)),
                ('modified_by', models.ForeignKey(blank=True, db_column='modified_by', null=True, on_delete=django.db.models.deletion.PROTECT, related_name='profit_modified_by', to=settings.AUTH_USER_MODEL)),
                ('operator_id', models.ForeignKey(db_column='operator_id', on_delete=django.db.models.deletion.PROTECT, related_name='profit_operator_id', to=settings.AUTH_USER_MODEL)),
                ('transaction_id', models.ForeignKey(db_column='transaction_id', on_delete=django.db.models.deletion.PROTECT, related_name='profit_transaction_id', to='transaction.Transaction')),
            ],
            options={
                'db_table': 'profit',
            },
        ),
    ]
