from django.conf import settings
from rest_framework import viewsets, mixins
from rest_framework.decorators import action
from django.shortcuts import get_object_or_404
from django.db.models import Q, Count, Sum, F
from datetime import date, datetime
from decimal import Decimal

from config.helper import MyResponse, code_generator, month_converter, MyResponseJson, day_converter

from transaction.models import Transaction, Profit, YearlyProfitView, MonthlyProfitView, DailyProfitView,\
                                MonthlyProfitProductView, YearlyProfitProductView, DailyProfitProductView,\
                                YearlyTransactionView, YearlyTransactionProductView, MonthlyTransactionView,\
                                MonthlyTransactionProductView, DailyTransactionView, DailyTransactionProductView
from product.models import Product
from user import permissions
from user.models import User
from transaction.serializers import TransactionCollectorSerializer, TransactionOperatorSerializer, ProfitSerializer,\
                                    YearlyProfitSerializer, MonthlyProfitSerializer, WeeklyProfitSerializer, \
                                    DaillyProfitSerializer, GraphOneSerializer, TransactionManagerSerializer,\
                                    ProfitManagerSerializer, GraphTwoSerializer
from django.contrib.auth import get_user

#phase 2
class TransactionCollectorViewSet(viewsets.ModelViewSet):
    queryset = Transaction.objects.all()
    serializer_class = TransactionCollectorSerializer


    def get_permissions(self):
        permission_classes = []
        if self.action == 'create' or self.action == 'list' or self.action == 'retrieve' or self.action == 'update' or self.action == 'partial_update':
            permission_classes = [permissions.CollectorOnly]
        elif self.action == 'destroy':
            permission_classes = [permissions.AdminOnly]
        return [permission() for permission in permission_classes]

    def list(self, request, *args, **kwargs):
        # user = get_user(request)
        user = request.user

        queryset = self.queryset
        queryset = queryset.filter(collector_id=str(user.id))

        serializer = self.get_serializer(queryset, many=True)
        data = {"list": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],"", data)

    def create(self, request, *args, **kwargs):
        # user = get_user(request)
        user = request.user

        request.data['collector_id'] = user.id
        code_type = 'TC-' + str(user.factory_id.id)

        identic = False
        while not identic:
            code = code_generator(code_type, stringLength=8)
            check_code = Transaction.objects.filter(code=code)
            identic = True if not check_code else False
        request.data['code'] = code
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by_id=user.id,
                        modified_by_id=user.id)

        data = {"detail": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                          "Pembelian berhasil dibuat.", data)

    def retrieve(self, request, pk=None):
        queryset = self.get_queryset()
        detail = get_object_or_404(queryset, pk=pk)
        serializer = TransactionOperatorSerializer(detail)

        response_data = {"detail": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"], "",
                          response_data)

    def update(self, request, pk=None):
        # user = get_user(request)
        user = request.user

        queryset = self.get_queryset()
        detail = get_object_or_404(queryset, pk=pk)
        if request.data['status'] == 'SUCCESS':
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "Kolektor tidak dapat menyelesaikan pembelian.", "")

        if detail.status == 'SUCCESS':
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "Pembelian sudah selesai.", "")

        if detail.status == 'CANCELED':
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "Pembelian sudah dibatalkan.", "")

        if detail.collector_id == user.id:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "Pembelian bukan milik anda", "")

        serializer = self.get_serializer(detail, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(modified_by_id=user.id)

        data = {"detail": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                          "Pembelian berhasil diperbarui.", data)


class TransactionOperatorViewSet(viewsets.ModelViewSet):
    queryset = Transaction.objects.all()
    serializer_class = TransactionOperatorSerializer

    def get_permissions(self):
        permission_classes = []
        if self.action == 'create' or self.action == 'list' or self.action == 'retrieve' or self.action == 'update' or self.action == 'partial_update':
            permission_classes = [permissions.OperatorOnly]
        elif self.action == 'destroy':
            permission_classes = [permissions.AdminOnly]
        return [permission() for permission in permission_classes]

    def list(self, request, *args, **kwargs):
        # user = get_user(request)
        user = request.user

        queryset = self.queryset
        filter_status = request.GET.get('status', None)

        if filter_status:
            queryset = queryset.filter(factory_id=str(user.factory_id.id), status=filter_status).\
                filter(Q(operator_id=str(user.id)) | Q(operator_id=None)).order_by('-date')

        else:
            queryset = queryset.filter(factory_id=str(user.factory_id.id)).filter(Q(operator_id=str(user.id)) | Q(operator_id=None)).\
                exclude(status=Transaction.CANCELED).order_by('-date')
            # queryset = queryset.filter(factory_id=str(user.factory_id.id)).filter(Q(operator_id=str(user.id)) | Q(operator_id=None))

        serializer = self.get_serializer(queryset, many=True)
        data = {"list": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],"", data)

    def create(self, request, *args, **kwargs):
        # user = get_user(request)
        user = request.user

        request.data['operator_id'] = user.id
        request.data['factory_id'] = user.factory_id.id
        request.data['status'] = 'SUCCESS'
        # request.data['date'] = date.today()
        request.data['time'] = datetime.now().time().strftime("%H:%M:%S")
        weight = request.data['weight']
        code_type = 'TO-' + str(user.factory_id.id)

        product = Product.objects.get(id=str(request.data['product_id']))

        identic = False
        while not identic:
            code = code_generator(code_type, stringLength=8)
            check_code = Transaction.objects.filter(code=code)
            identic = True if not check_code else False
        request.data['code'] = code

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by_id=user.id,
                        modified_by_id=user.id)

        data = {"detail": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                          "Pembelian berhasil dibuat.", data)

    def retrieve(self, request, pk=None):
        # user = get_user(request)
        user = request.user

        queryset = self.get_queryset()
        detail = get_object_or_404(queryset, pk=pk)

        serializer = TransactionOperatorSerializer(detail)
        if detail.factory_id != user.factory_id:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "Transaction is belong to another factory", "")

        if detail.operator_id != user.id:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "Pembelian dimiliki oleh operator lain", "")

        response_data = {"detail": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"], "",
                          response_data)

    def update(self, request, pk=None):
        # user = get_user(request)
        user = request.user

        request.data['operator_id'] = user.id
        queryset = self.get_queryset()
        detail = get_object_or_404(queryset, pk=pk)
        # if request.data['status'] == 'SUCCESS':
            # if not request.data['date']:
            # request.data['date'] = datetime.now()
            # request.data['time'] = datetime.now().time()

        if detail.status == 'SUCCESS':
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "Pembelian sudah selesai.", "")

        try:
            if detail.operator_id.id != user.id:
                return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                                  "Transaction already booked by another operator", "")
        except:
            pass

        if detail.factory_id != user.factory_id:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "Transaction is belong to another factory", "")

        serializer = self.get_serializer(detail, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(modified_by_id=user.id)

        data = {"detail": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                          "Pembelian berhasil diperbarui.", data)


class TransactionManagerViewSet(mixins.RetrieveModelMixin,
                                mixins.UpdateModelMixin,
                                mixins.ListModelMixin,
                                viewsets.GenericViewSet):
    queryset = Transaction.objects.all()
    serializer_class = TransactionManagerSerializer

    def get_permissions(self):
        permission_classes = []
        if self.action == 'list' or self.action == 'retrieve' or self.action == 'update' or self.action == 'partial_update':
            permission_classes = [permissions.ManagerOnly]
        elif self.action == 'destroy':
            permission_classes = [permissions.AdminOnly]
        return [permission() for permission in permission_classes]

    def list(self, request, *args, **kwargs):
        # user = get_user(request)
        user = request.user

        queryset = self.queryset
        filter_status = request.GET.get('status', None)
        filter_code = request.GET.get('code', None)
        filter_date = request.GET.get('date', None)
        filter_month = request.GET.get('month', None)
        filter_year = request.GET.get('year', None)

        if filter_month:
            filter_month=month_converter(filter_month)
            queryset = queryset.filter(date__month=filter_month)

        if filter_year:
            queryset = queryset.filter(date__year=filter_year)

        if filter_code:
            queryset = queryset.filter(code__icontains=filter_code)

        if filter_date:
            queryset = queryset.filter(date=filter_date)

        if filter_status:
            queryset = queryset.filter(factory_id=str(user.factory_id.id), status=filter_status).order_by('-date')

        else:
            queryset = queryset.filter(factory_id=str(user.factory_id.id)).\
                        exclude(status=Transaction.CANCELED).order_by('-date')
            # queryset = queryset.filter(factory_id=str(user.factory_id.id)).filter(Q(operator_id=str(user.id)) | Q(operator_id=None))

        serializer = self.get_serializer(queryset, many=True)
        data = {"list": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"], "",
                          data)

    def update(self, request, pk=None):
        # user = get_user(request)
        user = request.user

        queryset = self.get_queryset()
        detail = get_object_or_404(queryset, pk=pk)

        date_now = str(date.today()) + '-' + str(datetime.now().time().strftime("%H:%M:%S"))

        if not detail.notes_operator:
            request.data['notes_operator'] = ' \n(' + 'Pembelian dibatalkan oleh bapak/ibu ' + user.first_name + ' pada waktu ' + str(date_now) + ')'
        else:
            request.data['notes_operator'] = str(detail.notes_operator) + ' \n(' + 'Pembelian dibatalkan oleh bapak/ibu ' +\
                                             user.first_name + ' pada waktu ' + str(date_now) + ')'
        request.data['status'] = Transaction.CANCELED

        if detail.status == Transaction.CANCELED:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "Transaction is already canceled.", "")

        # try:
        #     if detail.operator_id.id != user.id:
        #         return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
        #                           "Transaction already booked by another operator", "")
        # except:
        #     pass

        if detail.factory_id != user.factory_id:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "Transaction is belong to another factory", "")

        serializer = self.get_serializer(detail, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(modified_by_id=user.id)

        data = {"detail": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                          "Pembelian berhasil dibatalkan.", data)

    def retrieve(self, request, pk=None):
        # user = get_user(request)
        user = request.user

        queryset = self.get_queryset()
        detail = get_object_or_404(queryset, pk=pk)
        serializer = TransactionManagerSerializer(detail)
        if detail.factory_id != user.factory_id:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "Transaction is belong to another factory", "")

        response_data = {"detail": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"], "",
                          response_data)

class ProfitViewSet(viewsets.ModelViewSet):
    # queryset = Profit.objects.all()
    # serializer_class = ProfitSerializer

    # def get_queryset(self):
    #     if self.action == 'yearly':
    #         return YearlyProfitView.objects.all()
    #     elif self.action == 'monthly':
    #         return MonthlyProfitView.objects.all()
    #     elif self.action == 'weekly' or self.action == 'daily':
    #         return DailyProfitView.objects.all()
    #     else:
    #         return Profit.objects.all()

    def get_serializer_class(self):
        if self.action == 'yearly':
            return YearlyProfitSerializer
        elif self.action == 'monthly':
            return MonthlyProfitSerializer
        elif self.action == 'weekly':
            return WeeklyProfitSerializer
        elif self.action == 'daily':
            return DaillyProfitSerializer
        elif self.action == 'graph_one':
            return GraphOneSerializer
        elif self.action == 'graph_two':
            return GraphTwoSerializer

        return ProfitSerializer

    def get_permissions(self):
        permission_classes = []
        if self.action == 'create' or self.action == 'list' or self.action == 'retrieve' or self.action == 'update' or self.action == 'partial_update':
            permission_classes = [permissions.OperatorOnly]
        elif self.action == 'yearly' or self.action == 'monthly' or self.action == 'weekly' or self.action == 'daily' or self.action == 'graph_one' \
                or self.action == 'graph_two' or self.action == 'yearly_detail' or self.action == 'monthly_detail' or self.action == 'weekly_detail' or self.action == 'graph_one_detail':
            permission_classes = [permissions.ManagerOnly]
        elif self.action == 'destroy':
            permission_classes = [permissions.AdminOnly]
        return [permission() for permission in permission_classes]

    def list(self, request, *args, **kwargs):
        # user = get_user(request)
        user = request.user
        filter_date = request.GET.get('date', None) #2020-02-11
        filter_status = request.GET.get('status', None)

        queryset = Profit.objects.all()
        queryset = queryset.filter(factory_id=str(user.factory_id.id), operator_id=user)

        if filter_date:
            queryset = queryset.filter(date=filter_date)

        if filter_status:
            queryset = queryset.filter(status=filter_status)
        else:
            queryset = queryset.filter(status=Profit.SUCCESS)

        serializer = self.get_serializer(queryset, many=True)
        data = {"list": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                          "", data)

    def create(self, request, *args, **kwargs):
        # user = get_user(request)
        user = request.user

        request.data['operator_id'] = user.id
        request.data['factory_id'] = user.factory_id.id
        nett_weight = request.data['nett_weight']
        date_profit = request.data['date']
        code_type = 'P-' + str(user.factory_id.id)

        product = Product.objects.get(id=str(request.data['product_id']))

        identic = False
        while not identic:
            code = code_generator(code_type, stringLength=8)
            check_code = Profit.objects.filter(code=code)
            identic = True if not check_code else False
        request.data['code'] = code

        try:
            if product.factory_id != user.factory_id:
                return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                                  "no matched product from your factory", "")

        except Product.DoesNotExist:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "product not found", "")

        request.data['price'] = product.price
        request.data['profit'] = round(product.price * Decimal(request.data['nett_weight']))

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by_id=user.id,
                        modified_by_id=user.id)

        data = {"detail": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                          "Hasil olah berhasil dibuat.", data)

    def retrieve(self, request, pk=None):
        # user = get_user(request)
        user = request.user

        queryset = Profit.objects.all()
        detail = get_object_or_404(queryset, pk=pk)
        # if str(detail.operator_id.id) != str(user.id):
        #     return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
        #                       "The transaction is not belongs to this user", "")

        if detail.factory_id != user.factory_id:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "Transaction is belongs to another factory", "")

        serializer = ProfitSerializer(detail)
        response_data = {"detail": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"], "",
                          response_data)

    def update(self, request, pk=None):
        # user = get_user(request)
        user = request.user

        request.data['operator_id'] = user.id
        nett_weight = request.data['nett_weight']
        date_profit = request.data['date']

        queryset = Profit.objects.all()
        detail = get_object_or_404(queryset, pk=pk)

        # get sum weight profit in one day
        current_profits = Profit.objects.filter(date=date_profit).exclude(id=pk)
        current_nett_weight = Decimal(nett_weight)
        for current_profit in current_profits:
            current_nett_weight += current_profit.nett_weight

        # get sum weight transaction in one day
        transactions = Transaction.objects.filter(date=date_profit, factory_id=user.factory_id)
        gross_weight = 0
        for transaction in transactions:
            gross_weight += transaction.weight

        if current_nett_weight > gross_weight:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "Gross weight can't be bigger than Current nett weight in one day", "")

        if str(detail.operator_id.id) != str(user.id):
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "The task is not belongs to this user", "")

        if detail.factory_id != user.factory_id:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "Task is belongs to another factory", "")

        try:
            # product = Product.objects.filter(name=str(request.data['product_name']), created_date__lte=trans_date).order_by('created_date').last()
            product = Product.objects.get(id=str(request.data['product_id']))
            if product.factory_id != user.factory_id:
                return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                                  "no matched product from your factory", "")

        except Product.DoesNotExist:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "product not found", "")

        request.data['price'] = product.price
        # request.data['profit'] = product.price * request.data['nett_weight']
        request.data['profit'] = round(product.price * Decimal(request.data['nett_weight']))

        serializer = self.get_serializer(detail, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(modified_by_id=user.id)

        data = {"detail": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                          "Pembelian berhasil diperbarui.", data)

    @action(detail=False)
    def yearly(self, request):
        # queryset = YearlyProfitView.objects.all()

        user = request.user
        filter_year = request.GET.get('year', None)

        queryset = YearlyProfitView.objects.all()
        queryset = queryset.filter(factory_id=str(user.factory_id.id))

        if filter_year:
            queryset = queryset.filter(year=filter_year)

        else:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "No Specific Date", "")

        yearly_transaction = YearlyTransactionView.objects.filter(year=filter_year, factory_id=str(user.factory_id.id)).first()
        if yearly_transaction:
            total_cost = yearly_transaction.total_cost
        else:
            total_cost = 0

        detail = get_object_or_404(queryset)
        serializer = YearlyProfitSerializer(detail, context={'total_cost': total_cost})
        data = {"detail": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                          "", data)

    @action(detail=False)
    def yearly_detail(self, request):
        # queryset = YearlyProfitView.objects.all()

        user = request.user
        filter_year = request.GET.get('year', None)

        queryset = YearlyProfitProductView.objects.all()
        queryset = queryset.filter(factory_id=str(user.factory_id.id))

        if filter_year:
            queryset = queryset.filter(year=filter_year).order_by('product_id')

        else:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "No Specific Date", "")

        products = Product.objects.filter(factory_id=str(user.factory_id.id)).order_by('id')

        data = []
        for product in products:

            queryset_product = queryset.filter(product_id=product).first()
            yearly_transaction = YearlyTransactionProductView.objects.filter(year=filter_year, factory_id=user.factory_id,\
                                                                            product_id=product.id).first()
            if yearly_transaction:
                total_cost = yearly_transaction.total_cost
            else:
                total_cost = 0

            if queryset_product:

                data.append({
                    'year': filter_year,
                    'factory_id': str(queryset_product.factory_id),
                    'product': queryset_product.product_id.name,
                    'total_cost': total_cost,
                    'total_profit': queryset_product.total_profit
                })

            else:
                data.append({
                    'year': filter_year,
                    'factory_id': str(user.factory_id.id),
                    'product': product.name,
                    'total_cost': total_cost,
                    'total_profit': 0
                })



        # detail = get_object_or_404(queryset)
        # serializer = self.get_serializer(detail)
        # data = {"detail": serializer.data}
        # return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
        #                   "", data)

        return MyResponseJson(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                              "", data)

    @action(detail=False)
    def monthly(self, request):
        user = request.user
        filter_month = request.GET.get('month', None)
        filter_year = request.GET.get('year', None)

        queryset = MonthlyProfitView.objects.all()
        queryset = queryset.filter(factory_id=str(user.factory_id.id))

        if filter_month and filter_year:
            filter_month = month_converter(filter_month)
            if filter_month < 4:
                filter_month_2 = 8 + filter_month
                filter_year_2 = int(filter_year) - 1
                queryset = queryset.filter(Q(month__lte=filter_month, month__gt=(filter_month - 4), year=filter_year) |\
                                           Q(month__gt=filter_month_2, month__lte=12, year=filter_year_2)).\
                                    order_by('-year', '-month')

            else:
                queryset = queryset.filter(month__lte=filter_month, month__gt=(filter_month - 4), year=filter_year).\
                                    order_by('-year', '-month')

        else:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "No Specific Date", "")

        #make it to a list and fix the month
        # if len(queryset) <= 3:
        list_queryset = list(queryset)


        dates = []
        filter_year_3 = int(filter_year)
        filter_month_3 = filter_month
        for x in range(3):
            if x == 0:
                month_year = [filter_month_3, filter_year_3]
                dates.append(month_year)
            filter_month_3 -= 1
            if filter_month_3 == 0:
                filter_month_3 = 12
                filter_year_3 -= 1
            month_year = [filter_month_3, filter_year_3]
            dates.append(month_year)

        i = 0
        lst = []
        x = 0
        for date in dates:
            try:
                # month_date = month_converter(date[0], 'reverse')
                monthly_transaction = MonthlyTransactionView.objects.filter(year=date[1], \
                                                                           month=date[0], \
                                                                           factory_id=user.factory_id).first()
                total_cost = monthly_transaction.cost if monthly_transaction else 0
                nett_weight = monthly_transaction.gross_weight if monthly_transaction else 0
                # test = list_queryset[x].month
                if date[0] != int(list_queryset[x].month):
                    dict_queryset = {
                        'month': date[0],
                        'year': date[1],
                        'factory_id': user.factory_id,
                        'nett_weight': nett_weight,
                        'cost': total_cost,
                        'profit': 0
                    }
                    lst.insert(i, dict_queryset)
                else:
                    dict_queryset = {
                        'month': date[0],
                        'year': date[1],
                        'factory_id': user.factory_id,
                        'nett_weight': nett_weight,
                        'cost': total_cost,
                        'profit': list_queryset[x].profit
                    }
                    lst.insert(i, dict_queryset)
                    x += 1
            except IndexError as e:
                # print(e)
                # month_date = month_converter(date[0])
                monthly_transaction = MonthlyTransactionView.objects.filter(year=date[1], \
                                                                           month=date[0], \
                                                                           factory_id=user.factory_id).first()
                total_cost = monthly_transaction.cost if monthly_transaction else 0
                nett_weight = monthly_transaction.gross_weight if monthly_transaction else 0

                dict_queryset = {
                    'month': date[0],
                    'year': date[1],
                    'factory_id': user.factory_id,
                    'nett_weight': nett_weight,
                    'cost': total_cost,
                    'profit': 0
                }
                lst.insert(i, dict_queryset)
            i += 1

        serializer = self.get_serializer(lst, many=True)
        data = {"list": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                          "", data)

        # serializer = self.get_serializer(queryset, many=True)
        #
        # data = {"list": serializer.data}
        # return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
        #                   "", data)

    @action(detail=False)
    def monthly_detail(self, request):
        user = request.user
        filter_month = request.GET.get('month', None)
        filter_year = request.GET.get('year', None)

        queryset = MonthlyProfitProductView.objects.all()
        queryset = queryset.filter(factory_id=str(user.factory_id.id))

        if filter_month and filter_year:
            filter_month = month_converter(filter_month)
            if filter_month < 4:
                filter_month_2 = 8 + filter_month
                filter_year_2 = int(filter_year) - 1
                queryset = queryset.filter(Q(month__lte=filter_month, month__gt=(filter_month - 4), year=filter_year) |\
                                           Q(month__gt=filter_month_2, month__lte=12, year=filter_year_2))
                                    # order_by('product_id', '-year', '-month')

            else:
                queryset = queryset.filter(month__lte=filter_month, month__gt=(filter_month - 4), year=filter_year)
                                    # order_by('product_id', '-year', '-month')

        else:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "No Specific Date", "")

        #make it to a list and fix the month
        # list_queryset = list(queryset)

        # get month
        dates = []
        filter_year_3 = int(filter_year)
        filter_month_3 = filter_month

        for x in range(3):
            if x == 0:
                month_year = [filter_month_3, filter_year_3]
                dates.append(month_year)
            filter_month_3 -= 1
            if filter_month_3 == 0:
                filter_month_3 = 12
                filter_year_3 -= 1
            month_year = [filter_month_3, filter_year_3]
            dates.append(month_year)

        # get product
        products = Product.objects.filter(factory_id=str(user.factory_id.id)).order_by('id')

        i = 0
        data = []
        for product in products:
            lst = []
            dct = {}
            list_queryset = list(queryset.filter(product_id=product).order_by('product_id', '-year', '-month'))
            x = 0

            first = True
            for date in dates:

                # check if product not active and ant transaction & profit
                if first:
                    first_year = dates[0][1]
                    last_year = dates[3][1]
                    first_month = dates[0][0]
                    last_month = dates[3][0]
                    product_transaction = MonthlyTransactionProductView.objects.filter(year__lte=first_year,
                                                                                       year__gte=last_year,
                                                                                       month__lte=first_month,
                                                                                       month__gte=last_month,
                                                                                       product_id=product.id,
                                                                                       factory_id=user.factory_id)

                    if not list_queryset and not product_transaction and product.status == Product.NOT_ACTIVE:
                        break
                    else:
                        first = False
                else:
                    first = False

                try:
                    # test = list_queryset[x].month
                    monthly_transaction = MonthlyTransactionProductView.objects.filter(year=date[1],
                                                                                       month=date[0],
                                                                                       product_id=product.id,
                                                                                       factory_id=user.factory_id).first()

                    total_cost = monthly_transaction.cost if monthly_transaction else 0

                    # aslinya gross_weight
                    nett_weight = monthly_transaction.gross_weight if monthly_transaction else 0

                    if date[0] != int(list_queryset[x].month):
                        dict_queryset = {
                            'month': month_converter(date[0], 'reverse'),
                            'year': date[1],
                            'factory_id': str(user.factory_id),
                            'nett_weight': nett_weight,
                            'cost': total_cost,
                            'profit': 0
                        }
                        lst.insert(i, dict_queryset)
                    else:
                        dict_queryset = {
                            'month': month_converter(date[0], 'reverse'),
                            'year': date[1],
                            'factory_id': str(user.factory_id),
                            'nett_weight': nett_weight,
                            'cost': total_cost,
                            'profit': list_queryset[x].profit
                        }
                        lst.insert(i, dict_queryset)
                        x += 1
                except IndexError as e:
                    print(e)
                    monthly_transaction = MonthlyTransactionProductView.objects.filter(year=date[1],
                                                                                       month=date[0],
                                                                                       product_id=product.id,
                                                                                       factory_id=user.factory_id).first()
                    total_cost = monthly_transaction.cost if monthly_transaction else 0

                    #aslinya gross_weight
                    nett_weight = monthly_transaction.gross_weight if monthly_transaction else 0

                    dict_queryset = {
                        'month': month_converter(date[0], 'reverse'),
                        'year': date[1],
                        'factory_id': str(user.factory_id),
                        'nett_weight': nett_weight,
                        'cost': total_cost,
                        'profit': 0
                    }
                    lst.insert(i, dict_queryset)
                i += 1

            if first:
                continue

            dct['product_id'] = str(product.id)
            dct['product_name'] = str(product.name)
            dct['detail'] = lst
            data.append(dct)

        # serializer = self.get_serializer(serialist, many=True)
        # data = {"list": serializer.data}


        return MyResponseJson(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                          "", data)
        #
        # serializer = self.get_serializer(queryset, many=True)
        #
        # data = {"list": serializer.data}
        # return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
        #                   "", data)

    @action(detail=False)
    def weekly(self, request):
        user = request.user
        filter_month = request.GET.get('month', None)
        filter_year = request.GET.get('year', None)

        queryset = DailyProfitView.objects.all()
        queryset = queryset.filter(factory_id=str(user.factory_id.id))

        if filter_month and filter_year:
            filter_month = month_converter(filter_month)
            queryset = queryset.filter(month=filter_month, year=filter_year).\
                                annotate(num_week = Count('week')).\
                                values('week').order_by('week').\
                                annotate(total_purchase=Sum('purchase')).\
                                annotate(total_profit=Sum('profit')).\
                                annotate(total_weight=Sum('nett_weight'))

            i = 0
            data = []

            for x in range(5):
                y = x + 1
                weekly = DailyTransactionView.objects.filter(year=filter_year, \
                                                            month=filter_month, \
                                                            week=y,\
                                                            factory_id=user.factory_id)
                total_cost = 0
                gross_weight = 0
                for daily in weekly:
                    daily_cost = daily.cost
                    total_cost += daily_cost

                    daily_weight = daily.gross_weight
                    gross_weight += daily_weight

                try:
                    l = queryset[i]['week']
                    if int(y) == int(queryset[i]['week']):
                        data.append({
                            "week": str(y),
                            "total_cost": total_cost,
                            "total_profit": queryset[i]['total_profit'],
                            "total_weight": gross_weight
                        })
                        i += 1
                    else:
                        data.append({
                            "week": str(y),
                            "total_cost": total_cost,
                            "total_profit": 0,
                            "total_weight": gross_weight
                        })
                except IndexError:
                    data.append({
                        "week": str(y),
                        "total_cost": total_cost,
                        "total_profit": 0,
                        "total_weight": gross_weight
                    })
        else:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "No Specific Date", "")

        return MyResponseJson(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                              "", data)

    @action(detail=False)
    def weekly_detail(self, request):
        user = request.user
        filter_month = request.GET.get('month', None)
        filter_year = request.GET.get('year', None)

        queryset = DailyProfitProductView.objects.all()
        products = Product.objects.filter(factory_id=str(user.factory_id.id)).order_by('id')

        if filter_month and filter_year:
            data = []
            for product in products:
                dct = {}
                queryset_week = queryset.filter(factory_id=str(user.factory_id.id), product_id=str(product.id))

                filter_month_2 = month_converter(filter_month)
                queryset_week = queryset_week.filter(month=filter_month_2, year=filter_year). \
                    annotate(num_week=Count('week')). \
                    values('week').order_by('week'). \
                    annotate(total_purchase=Sum('purchase')). \
                    annotate(total_profit=Sum('profit')). \
                    annotate(total_weight=Sum('nett_weight'))
                i = 0
                lst = []

                product_transaction = DailyTransactionProductView.objects.filter(year=filter_year,
                                                                                 month=filter_month_2,
                                                                                 product_id=product.id,
                                                                                 factory_id=user.factory_id)

                if not queryset_week and not product_transaction and product.status == Product.NOT_ACTIVE:
                    continue

                for x in range(5):
                    y = x + 1
                    weekly = DailyTransactionProductView.objects.filter(year=filter_year, \
                                                                 month=filter_month_2, \
                                                                 week=y, \
                                                                 product_id=product.id, \
                                                                 factory_id=user.factory_id)
                    total_cost = 0
                    gross_weight = 0
                    for daily in weekly:
                        daily_cost = daily.cost
                        total_cost += daily_cost

                        daily_weight = daily.gross_weight
                        gross_weight += daily_weight

                    try:
                        l = queryset_week[i]['week']
                        if int(y) == int(queryset_week[i]['week']):
                            lst.append({
                                "week": str(y),
                                "total_cost": total_cost,
                                "total_profit": queryset_week[i]['total_profit'],
                                "total_weight": gross_weight
                            })
                            i =+ 1
                        else:
                            lst.append({
                                "week": str(y),
                                "total_cost": total_cost,
                                "total_profit": 0,
                                "total_weight": gross_weight
                            })
                    except IndexError:
                        lst.append({
                            "week": str(y),
                            "total_cost": total_cost,
                            "total_profit": 0,
                            "total_weight": gross_weight
                        })
                dct['product_id'] = str(product.id)
                dct['product_name'] = str(product.name)
                dct['detail'] = lst
                data.append(dct)

        else:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "No Specific Date", "")


        return MyResponseJson(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                              "", data)

    @action(detail=False)
    def daily(self, request):
        user = request.user
        filter_month = request.GET.get('month', None)
        filter_year = request.GET.get('year', None)
        filter_week = request.GET.get('week', None)
        filter_product = request.GET.get('product', None)

        if filter_product:
            queryset = DailyProfitProductView.objects.all()
            queryset = queryset.filter(factory_id=str(user.factory_id.id), product_id=filter_product)
        else:
            queryset = DailyProfitView.objects.all()
            queryset = queryset.filter(factory_id=str(user.factory_id.id))

        if filter_month and filter_year and filter_week:
            filter_month = month_converter(filter_month)

            queryset = queryset.filter(month=filter_month, year=filter_year, week=filter_week). \
                annotate(num_week=Count('dow')). \
                values('dow', 'date').order_by('date'). \
                annotate(total_purchase=Sum('purchase')). \
                annotate(total_profit=Sum('profit')). \
                annotate(total_weight=Sum('nett_weight'))\

            data = []
            i = 0
            for x in range(7):
                y = x + 1
                if y == 7:
                    y = 0
                if filter_product:
                    weekly_transaction = DailyTransactionProductView.objects.filter(year=filter_year,
                                                                             month=filter_month,
                                                                             week=filter_week,
                                                                             dow=y,
                                                                             product_id=filter_product,
                                                                             factory_id=user.factory_id).first()
                else:
                    weekly_transaction = DailyTransactionView.objects.filter(year=filter_year,
                                                                 month=filter_month,
                                                                 week=filter_week,
                                                                 dow=y,
                                                                 factory_id=user.factory_id).first()

                total_cost = weekly_transaction.cost if weekly_transaction else 0
                gross_weight = weekly_transaction.gross_weight if weekly_transaction else 0

                try:
                    if int(y) == int(queryset[i]['dow']):
                        data.append({
                            "date": queryset[i]['date'],
                            "day": day_converter(y),
                            "total_cost": total_cost,
                            "total_profit": queryset[i]['total_profit'],
                            "total_weight": gross_weight
                        })
                        i += 1
                    else:
                        if total_cost:
                            data.append({
                                "date": weekly_transaction.date,
                                "day": day_converter(y),
                                "total_cost": total_cost,
                                "total_profit": 0,
                                "total_weight": gross_weight
                            })
                except IndexError:
                    gross_weight = weekly_transaction.gross_weight if weekly_transaction else 0

                    if total_cost:
                        data.append({
                            "date": weekly_transaction.date,
                            "day": day_converter(y),
                            "total_cost": total_cost,
                            "total_profit": 0,
                            "total_weight": gross_weight
                        })
        else:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "No Specific Date", "")

        return MyResponseJson(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                              "", data)

    # @action(detail=False)
    # def daily_detail(self, request):
    #     user = request.user
    #     filter_month = request.GET.get('month', None)
    #     filter_year = request.GET.get('year', None)
    #     filter_week = request.GET.get('week', None)
    #
    #     queryset = DailyProfitProductView.objects.all()
    #     products = Product.objects.filter(factory_id=str(user.factory_id.id)).order_by('id')
    #
    #     queryset = queryset.filter(factory_id=str(user.factory_id.id))
    #
    #     if filter_month and filter_year:
    #         data = []
    #         for product in products:
    #             dct = {}
    #             queryset = queryset.filter(factory_id=str(user.factory_id.id), product_id=str(product.id))
    #
    #             filter_month_2 = month_converter(filter_month)
    #             queryset_dow = queryset.filter(month=filter_month_2, year=filter_year, week=filter_week). \
    #                 annotate(num_week=Count('dow')). \
    #                 values('dow').order_by('date'). \
    #                 annotate(total_purchase=Sum('purchase')). \
    #                 annotate(total_profit=Sum('profit')). \
    #                 annotate(total_weight=Sum('nett_weight'))
    #
    #             i = 0
    #             lst = []
    #             for x in range(7):
    #                 y = x + 1
    #                 try:
    #                     # l = queryset_dow[i]['dow']
    #                     if int(y) == int(queryset_dow[i]['dow']):
    #                         lst.append({
    #                             "day": day_converter(y),
    #                             "total_purchase": queryset_dow[i]['total_purchase'],
    #                             "total_profit": queryset_dow[i]['total_profit'],
    #                             "total_weight": queryset_dow[i]['total_weight']
    #                         })
    #                         i =+ 1
    #                     else:
    #                         lst.append({
    #                             "day": day_converter(y),
    #                             "total_purchase": 0,
    #                             "total_profit": 0,
    #                             "total_weight": 0
    #                         })
    #                 except IndexError:
    #                     lst.append({
    #                         "day": day_converter(y),
    #                         "total_purchase": 0,
    #                         "total_profit": 0,
    #                         "total_weight": 0
    #                     })
    #             dct['product_id'] = str(product.id)
    #             dct['product_name'] = str(product.name)
    #             dct['detail'] = lst
    #             data.append(dct)
    #
    #     else:
    #         return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
    #                           "No Specific Date", "")
    #
    #
    #     return MyResponseJson(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
    #                           "", data)

    @action(detail=False)
    def graph_one(self, request):
        user = request.user
        filter_month = request.GET.get('month', None)
        filter_year = request.GET.get('year', None)

        queryset = MonthlyProfitView.objects.all()
        queryset = queryset.filter(factory_id=str(user.factory_id.id))

        # if filter_month and filter_year:
        #     filter_month = month_converter(filter_month)
        #     if filter_month < 4:
        #         filter_month_2 = 8 + filter_month
        #         filter_year_2 = int(filter_year) - 1
        #         queryset = queryset.filter(Q(month__lte=filter_month, month__gt=(filter_month - 4), year=filter_year) |\
        #                                    Q(month__gt=filter_month_2, month__lte=12, year=filter_year_2)).\
        #                             order_by('-year', '-month')
        #
        #     else:
        #         queryset = queryset.filter(month__lte=filter_month, month__gt=(filter_month - 4), year=filter_year).\
        #                             order_by('-year', '-month')

        if filter_year:
            queryset = queryset.filter(year=filter_year).order_by('month')

            lst = list(queryset)
            serialist = []
            i = 0
            for x in range(1, 13):
                month = month_converter(x, 'reverse')
                try:
                    if x != lst[i].month:
                        serialist.append ({
                            'id' : x,
                            'month': month,
                            'percentage' : 0.00
                        })
                    else:
                        nett_weight = lst[i].nett_weight
                        # transactions = Transaction.objects.filter(factory_id=lst[i].factory_id, status=Transaction.SUCCESS,
                        #                                           date__month=lst[i].month)
                        # gross_weight = 0

                        # transaction = MonthlyTransactionView.filter(factory_id=lst[i].factory_id, month=lst[i].month,
                        #                                             year=filter_year)

                        try:

                            # for transaction in transactions:
                            #     gross_weight += transaction.weight

                            transaction = MonthlyTransactionView.objects.filter(factory_id=lst[i].factory_id,
                                                                        month=lst[i].month,
                                                                        year=filter_year).first()
                            gross_weight = transaction.gross_weight if transaction else 0

                            percentage = nett_weight / gross_weight
                        except ZeroDivisionError:
                            # return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"],
                            #                   settings.RESPONSE_META["FAILED"]["STATUS"],
                            #                   "Eror in Division", "")
                            percentage = 0.00

                        serialist.append({
                            'id': x,
                            'month': month,
                            'percentage': percentage
                        })
                        i += 1

                except IndexError:
                    serialist.append({
                        'id': x,
                        'month': month,
                        'percentage': 0.00
                    })

        else:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "No Specific Date", "")

        serializer = self.get_serializer(serialist, many=True)
        data = {"list": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                          "", data)

    @action(detail=False)
    def graph_one_detail(self, request):
        user = request.user
        # filter_month = request.GET.get('month', None)
        filter_year = request.GET.get('year', None)
        products = Product.objects.filter(factory_id=str(user.factory_id.id)).order_by('id')

        queryset = MonthlyProfitProductView.objects.all()
        queryset = queryset.filter(factory_id=str(user.factory_id.id))

        if filter_year:
            data = []
            for product in products:
                dct = {}
                queryset_graph = queryset.filter(year=filter_year, product_id=str(product.id)).order_by('month')

                i = 0
                lst = []
                show = False
                if product.status == Product.ACTIVE:
                    show = True

                for x in range(12):
                    y = x + 1
                    month = month_converter(y, 'reverse')
                    try:
                        # l = queryset_dow[i]['dow']
                        if int(y) == int(queryset_graph[i].month):
                            nett_weight = queryset_graph[i].nett_weight
                            try:
                                transaction = MonthlyTransactionProductView.objects.filter(factory_id=user.factory_id,
                                                                                           month=queryset_graph[i].month,
                                                                                           year=filter_year,
                                                                                           product_id=product.id).first()
                                gross_weight = transaction.gross_weight if transaction else 0

                                percentage = nett_weight / gross_weight
                                show = True
                            except ZeroDivisionError:
                                percentage = 0.00

                            lst.append({
                                'id': y,
                                'month': month,
                                'percentage': percentage,
                            })
                            i += 1
                        else:
                            lst.append({
                                'id': y,
                                'month': month,
                                'percentage': 0.00,
                            })
                    except IndexError:
                        lst.append({
                            'id': y,
                            'month': month,
                            'percentage': 0.00
                        })

                if show == False:
                    continue

                dct['product_id'] = str(product.id)
                dct['product_name'] = str(product.name)
                dct['detail'] = lst
                data.append(dct)

        else:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "No Specific Date", "")

        return MyResponseJson(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                              "", data)

    @action(detail=False)
    def graph_two(self, request):
        user = request.user
        filter_month = request.GET.get('month', None)
        filter_month = month_converter(filter_month)
        filter_year = request.GET.get('year', None)

        queryset = MonthlyProfitProductView.objects.all()
        queryset = queryset.filter(factory_id=str(user.factory_id.id), month=filter_month, year=filter_year)

        queryset_weight = list(queryset.annotate(factory = Count('factory_id')).values('factory_id').\
                               annotate(total_weight=Sum('nett_weight')).values('total_weight'))

        if queryset_weight:
            total_weight = queryset_weight[0]['total_weight']

        # if no profit in a month
        else:
            serialist = []
            products = Product.objects.filter(factory_id=str(user.factory_id.id), status=Product.ACTIVE)
            for product in products:
                serialist.append({
                    'month': filter_month,
                    'year': filter_year,
                    'factory_id': user.factory_id,
                    "nett_weight": 0,
                    "product": {
                        "id": product.id,
                        "name": product.name
                    },
                    "percentage": 0.00
                })

            serializer = self.get_serializer(serialist, many=True)

            data = {"list": serializer.data}
            return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                              "", data)

        queryset =queryset.order_by('-nett_weight'). \
                    annotate(percentage=F('nett_weight') / total_weight)

        lsts = list(queryset)
        serialist = []
        prod = []
        for lst in lsts:
            serialist.append({
                'month': filter_month,
                'year': filter_year,
                'factory_id': user.factory_id,
                "nett_weight": lst.nett_weight,
                "product": {
                    "id": lst.product_id.id,
                    "name": lst.product_id.name
                },
                "percentage": lst.percentage
            })
            prod.append(lst.product_id.name)

        products = Product.objects.filter(factory_id=str(user.factory_id.id), status=Product.ACTIVE)
        for product in products:
            if product.name not in prod:
                serialist.append({
                    'month':filter_month,
                    'year': filter_year,
                    'factory_id': user.factory_id,
                    "nett_weight": 0,
                    "product": {
                        "id": product.id,
                        "name": product.name
                    },
                    "percentage": 0.00
                })

        serializer = self.get_serializer(serialist, many=True)

        data = {"list": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                          "", data)


class ProfitManagerViewSet(mixins.RetrieveModelMixin,
                                mixins.UpdateModelMixin,
                                mixins.ListModelMixin,
                                viewsets.GenericViewSet):
    queryset = Profit.objects.all()
    serializer_class = ProfitManagerSerializer

    def get_permissions(self):
        permission_classes = []
        if self.action == 'list' or self.action == 'retrieve' or self.action == 'update' or self.action == 'partial_update':
            permission_classes = [permissions.ManagerOnly]
        elif self.action == 'destroy':
            permission_classes = [permissions.AdminOnly]
        return [permission() for permission in permission_classes]

    def list(self, request, *args, **kwargs):
        # user = get_user(request)
        user = request.user

        queryset = self.queryset
        filter_status = request.GET.get('status', None)
        filter_code = request.GET.get('code', None)
        filter_date = request.GET.get('date', None)
        filter_month = request.GET.get('month', None)
        filter_year = request.GET.get('year', None)

        if filter_month:
            filter_month = month_converter(filter_month)
            queryset = queryset.filter(date__month=filter_month)

        if filter_year:
            queryset = queryset.filter(date__year=filter_year)

        if filter_code:
            queryset = queryset.filter(code__icontains=filter_code)

        if filter_date:
            queryset = queryset.filter(date=filter_date)

        if filter_status:
            queryset = queryset.filter(factory_id=str(user.factory_id.id), status=filter_status).order_by('-date')

        else:
            queryset = queryset.filter(factory_id=str(user.factory_id.id)).\
                        exclude(status=Profit.CANCELED).order_by('-date')
            # queryset = queryset.filter(factory_id=str(user.factory_id.id)).filter(Q(operator_id=str(user.id)) | Q(operator_id=None))

        serializer = self.get_serializer(queryset, many=True)
        data = {"list": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"], "",
                          data)

    def update(self, request, pk=None):
        # user = get_user(request)
        user = request.user

        queryset = self.get_queryset()
        detail = get_object_or_404(queryset, pk=pk)

        date_now = str(date.today()) + '-' + str(datetime.now().time().strftime("%H:%M:%S"))

        if not detail.note:
            request.data['note'] =' \n(' + 'Pembelian dibatalkan oleh bapak/ibu ' + user.first_name + ' pada waktu ' + str(date_now) + ')'
        else:
            request.data['note'] = str(detail.note) + ' \n(' + 'Pembelian dibatalkan oleh bapak/ibu ' + user.first_name + ' pada waktu ' + str(date_now) + ')'

        request.data['status'] = Profit.CANCELED

        if detail.status == Profit.CANCELED:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "Profit is already canceled.", "")

        # try:
        #     if detail.operator_id.id != user.id:
        #         return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
        #                           "Transaction already booked by another operator", "")
        # except:
        #     pass

        if detail.factory_id != user.factory_id:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "Profit is belong to another factory", "")

        datal = request.data
        serializer = self.get_serializer(detail, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(modified_by_id=user.id)

        data = {"detail": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                          "Hasil olah berhasil dibatalkan.", data)

    def retrieve(self, request, pk=None):
        # user = get_user(request)
        user = request.user

        queryset = Profit.objects.all()
        detail = get_object_or_404(queryset, pk=pk)

        if detail.factory_id != user.factory_id:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "Profit is belongs to another factory", "")

        serializer = ProfitSerializer(detail)
        response_data = {"detail": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"], "",
                          response_data)