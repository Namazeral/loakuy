from django.urls import path, include
from rest_framework import routers
from transaction import views, models


router = routers.DefaultRouter()
router.register(r'collector', views.TransactionCollectorViewSet)
router.register(r'operator', views.TransactionOperatorViewSet)
router.register(r'manager', views.TransactionManagerViewSet)
router.register(r'profit', views.ProfitViewSet, basename=models.Profit)
router.register(r'profitmanager', views.ProfitManagerViewSet)

urlpatterns = [
    path('', include(router.urls)),
]