from django.conf import settings
from decimal import Decimal
from rest_framework import serializers
from rest_framework.exceptions import APIException
import pytz

from user.models import User
from transaction.models import Transaction, Profit, YearlyProfitView, MonthlyProfitView, DailyProfitView, MonthlyProfitProductView

from config.helper import day_converter, month_converter, MyResponse

class TransactionCollectorSerializer(serializers.ModelSerializer):
    created_by = serializers.SerializerMethodField()
    modified_by = serializers.SerializerMethodField()
    factory = serializers.SerializerMethodField()
    collector = serializers.SerializerMethodField()
    operator = serializers.SerializerMethodField()
    notes = serializers.SerializerMethodField()

    class Meta:
        model = Transaction
        fields = ('id', 'collector', 'collector_id','factory', 'factory_id', 'operator', 'operator_id','weight', 'code',
                  'notes', 'notes_operator', 'notes_collector', 'cost', 'status', 'time', 'date', 'created_date', 'created_by',
                  'modified_date', 'modified_by')
        extra_kwargs = {'operator_id': {'read_only': True},
                        'notes_operator': {'read_only': True},
                        'date': {'read_only': True},
                        'time': {'read_only': True},
                        'id': {'read_only': True}}

    def get_created_by(self, obj):
        users_obj = getattr(obj, "created_by", None)
        return '{}'.format(getattr(users_obj, "first_name", None)) if users_obj is not None else None

    def get_modified_by(self, obj):
        users_obj = getattr(obj, "created_by", None)
        return '{}'.format(getattr(users_obj, "first_name", None)) if users_obj is not None else None

    def get_factory(self, obj):
        factory = {
            'factory_id': obj.factory_id.id,
            'factory_name': obj.factory_id.name,
            'address': obj.factory_id.address,
            'longitude': obj.factory_id.longitude,
            'latitude': obj.factory_id.latitude,
        }
        return factory

    def get_operator(self, obj):
        try:
            operator = {
                'operator_id': obj.operator_id.id,
                'operator_name': obj.operator_id.name,
                'operator_phone': obj.operator_id.mobile_number
            }
        except:
            operator = {
                'operator_id': None,
                'operator_name': None,
                'operator_phone': None
            }
        return operator

    def get_notes(self, obj):
        notes = {
            'notes_collector': obj.notes_collector,
            'notes_operator': obj.notes_operator,
        }
        return notes

    def get_collector(self, obj):
        collector = {
            'collector_id': obj.collector_id.id,
            'collector_name': obj.collector_id.first_name,
        }
        return collector

    def update(self, instance, validated_data):
        instance.notes_collector = validated_data.get('notes_collector', instance.notes_collector)
        instance.status = validated_data.get('status', instance.status)
        instance.save()

        return instance

class TransactionOperatorSerializer(serializers.ModelSerializer):
    created_by = serializers.SerializerMethodField()
    modified_by = serializers.SerializerMethodField()
    factory = serializers.SerializerMethodField()
    product = serializers.SerializerMethodField()
    collector = serializers.SerializerMethodField()
    operator = serializers.SerializerMethodField()
    notes = serializers.SerializerMethodField()

    class Meta:
        model = Transaction
        fields = ('id', 'collector', 'collector_id', 'factory', 'factory_id', 'operator', 'operator_id', 'product', 'product_id', 'weight', 'code',
                  'notes', 'notes_collector','notes_operator', 'cost', 'status', 'time', 'date',
                  'created_date', 'created_by', 'modified_date', 'modified_by')
        extra_kwargs = {'collector_id': {'read_only': True},
                        'notes_collector': {'read_only': True},
                        'id': {'read_only': True},
                        'product_id': {'write_only': True}
                        }

    def get_created_by(self, obj):
        users_obj = getattr(obj, "created_by", None)
        return '{}'.format(getattr(users_obj, "first_name", None)) if users_obj is not None else None

    def get_modified_by(self, obj):
        users_obj = getattr(obj, "created_by", None)
        return '{}'.format(getattr(users_obj, "first_name", None)) if users_obj is not None else None

    def get_factory(self, obj):
        factory = {
            'factory_id': obj.factory_id.id,
            'factory_name': obj.factory_id.name,
            'address': obj.factory_id.address,
            'longitude': obj.factory_id.longitude,
            'latitude': obj.factory_id.latitude,
        }
        return factory

    def get_product(self, obj):
        try:
            product = {
                'product_id': obj.product_id.id,
                'product_name': obj.product_id.name,
            }
        except:
            product = ""
        return product

    def get_operator(self, obj):
        try:
            operator = {
                'operator_id': obj.operator_id.id,
                'operator_name': obj.operator_id.first_name,
                'operator_phone': obj.operator_id.mobile_number,
            }
        except:
            operator = {
                'operator_id': None,
                'operator_name': None,
                'operator_phone': None,
            }
        return operator

    def get_notes(self, obj):
        notes = {
            'notes_collector': obj.notes_collector,
            'notes_operator': obj.notes_operator,
        }
        return notes

    def get_collector(self, obj):
        try:
            collector = {
                'collector_id': obj.collector_id.id,
                'collector_name': obj.collector_id.first_name,
            }
        except:
            collector = {
                'collector_id': None,
                'collector_name': None,
            }
        return collector

    def update(self, instance, validated_data):
        instance.notes_operator = validated_data.get('notes_operator', instance.notes_operator)
        instance.operator_id = validated_data.get('operator_id', instance.operator_id)
        # instance.time = validated_data.get('time', instance.time)
        # instance.date = validated_data.get('date', instance.date)
        instance.status = validated_data.get('status', instance.status)
        instance.save()

        return instance

class TransactionManagerSerializer(serializers.ModelSerializer):
    created_by = serializers.SerializerMethodField()
    modified_by = serializers.SerializerMethodField()
    product = serializers.SerializerMethodField()
    factory = serializers.SerializerMethodField()
    collector = serializers.SerializerMethodField()
    operator = serializers.SerializerMethodField()
    notes = serializers.SerializerMethodField()

    class Meta:
        model = Transaction
        fields = ('id', 'collector', 'collector_id', 'factory', 'factory_id', 'operator', 'operator_id', 'product', 'product_id', 'weight', 'code',
                  'notes', 'notes_collector','notes_operator', 'cost', 'status', 'time', 'date',
                  'created_date', 'created_by', 'modified_date', 'modified_by')
        extra_kwargs = {'collector_id': {'read_only': True},
                        'notes_collector': {'read_only': True},
                        'id': {'read_only': True},
                        'product_id': {'write_only': True}}

    def get_created_by(self, obj):
        users_obj = getattr(obj, "created_by", None)
        return '{}'.format(getattr(users_obj, "first_name", None)) if users_obj is not None else None

    def get_modified_by(self, obj):
        users_obj = getattr(obj, "created_by", None)
        return '{}'.format(getattr(users_obj, "first_name", None)) if users_obj is not None else None

    def get_factory(self, obj):
        factory = {
            'factory_id': obj.factory_id.id,
            'factory_name': obj.factory_id.name,
            'address': obj.factory_id.address,
            'longitude': obj.factory_id.longitude,
            'latitude': obj.factory_id.latitude,
        }
        return factory

    def get_operator(self, obj):
        try:
            operator = {
                'operator_id': obj.operator_id.id,
                'operator_name': obj.operator_id.first_name,
                'operator_phone': obj.operator_id.mobile_number
            }
        except:
            operator = {
                'operator_id': None,
                'operator_name': None,
                'operator_phone': None
            }
        return operator

    def get_notes(self, obj):
        notes = {
            'notes_collector': obj.notes_collector,
            'notes_operator': obj.notes_operator,
        }
        return notes

    def get_collector(self, obj):
        try:
            collector = {
                'collector_id': obj.collector_id.id,
                'collector_name': obj.collector_id.first_name,
            }
        except:
            collector = {
                'collector_id': None,
                'collector_name': None,
            }
        return collector

    def get_product(self, obj):
        try:
            product = {
                'product_id': obj.product_id.id,
                'product_name': obj.product_id.name,
            }
        except:
            product = ""
        return product

    def update(self, instance, validated_data):
        instance.notes_operator = validated_data.get('notes_operator', instance.notes_operator)
        instance.status = validated_data.get('status', instance.status)
        instance.save()

        return instance

class ProfitSerializer(serializers.ModelSerializer):
    time = serializers.SerializerMethodField()
    product = serializers.SerializerMethodField()
    operator = serializers.SerializerMethodField()
    factory = serializers.SerializerMethodField()

    class Meta:
        model = Profit
        fields = ('id', 'nett_weight', 'profit', 'price', 'date', 'operator', 'operator_id', 'product', 'product_id', 'factory', 'factory_id', 'code',
                  'created_date', 'created_by', 'modified_date', 'modified_by', 'time', 'status', 'note')
        extra_kwargs = {'id': {'read_only': True},
                        'profit': {'write_only': True},
                        'price': {'write_only': True},
                        'time': {'read_only': True}
                        }

    def get_time(self, obj):
        local_tz = pytz.timezone('Asia/Jakarta')
        time = obj.created_date.astimezone(local_tz).strftime("%H:%M:%S")
        return time

    def get_operator(self, obj):
        try:
            operator = {
                'operator_id': obj.operator_id.id,
                'operator_name': obj.operator_id.first_name,
                'operator_phone': obj.operator_id.mobile_number
            }
        except:
            operator = {
                'operator_id': None,
                'operator_name': None,
                'operator_phone': None,
            }
        return operator

    def get_factory(self, obj):
        factory = {
            'factory_id': obj.factory_id.id,
            'factory_name': obj.factory_id.name,
            'address': obj.factory_id.address,
            'longitude': obj.factory_id.longitude,
            'latitude': obj.factory_id.latitude,
        }
        return factory

    def get_product(self, obj):
        try:
            product = {
                'product_id': obj.product_id.id,
                'product_name': obj.product_id.name,
            }
        except:
            product = ""
        return product

    def update(self, instance, validated_data):
        instance.nett_weight = validated_data.get('nett_weight', instance.nett_weight)
        instance.profit = validated_data.get('profit', instance.profit)
        instance.price = validated_data.get('price', instance.price)
        instance.product_id = validated_data.get('product_id', instance.product_id)
        instance.save()

        return instance

class ProfitManagerSerializer(serializers.ModelSerializer):
    time = serializers.SerializerMethodField()
    product = serializers.SerializerMethodField()
    operator = serializers.SerializerMethodField()
    factory = serializers.SerializerMethodField()

    class Meta:
        model = Profit
        fields = ('id', 'nett_weight', 'profit', 'price', 'date', 'operator', 'operator_id', 'product', 'product_id', 'factory', 'factory_id', 'code',
                  'created_date', 'created_by', 'modified_date', 'modified_by', 'time', 'status', 'note')
        extra_kwargs = {'id': {'read_only': True},
                        'time': {'read_only': True},
                        'operator_id': {'read_only': True}
                        }

    def get_time(self, obj):
        local_tz = pytz.timezone('Asia/Jakarta')
        time = obj.created_date.astimezone(local_tz).strftime("%H:%M:%S")
        return time

    def get_operator(self, obj):
        try:
            operator = {
                'operator_id': obj.operator_id.id,
                'operator_name': obj.operator_id.first_name,
                'operator_phone': obj.operator_id.mobile_number,
            }
        except:
            operator = {
                'operator_id': None,
                'operator_name': None,
                'operator_phone': None
            }
        return operator

    def get_factory(self, obj):
        factory = {
            'factory_id': obj.factory_id.id,
            'factory_name': obj.factory_id.name,
            'address': obj.factory_id.address,
            'longitude': obj.factory_id.longitude,
            'latitude': obj.factory_id.latitude,
        }
        return factory

    def get_product(self, obj):
        try:
            product = {
                'product_id': obj.product_id.id,
                'product_name': obj.product_id.name,
            }
        except:
            product = ""
        return product

    def update(self, instance, validated_data):
        instance.status = validated_data.get('status', instance.nett_weight)
        instance.note = validated_data.get('note', instance.nett_weight)
        instance.save()

        return instance

class YearlyProfitSerializer(serializers.ModelSerializer):
    total_cost = serializers.SerializerMethodField()

    class Meta:
        model = YearlyProfitView
        # fields = '__all__'
        fields = ('id', 'year', 'factory_id', 'total_cost', 'total_profit')
        extra_kwargs = {'id': {'read_only': True}
                        }

    def get_total_cost(self, obj):
        total_cost = self.context.get("total_cost")
        return total_cost


class MonthlyProfitSerializer(serializers.ModelSerializer):
    month = serializers.SerializerMethodField()
    cost = serializers.SerializerMethodField()

    class Meta:
        model = MonthlyProfitView
        # fields = '__all__'
        fields = ('month', 'year', 'factory_id', 'nett_weight', 'cost', 'profit')
        # extra_kwargs = {'id': {'read_only': True}
        #                 }

    def get_month(self, obj):
        # try:
            month = obj['month']
            return month_converter(month, 'reverse')
        # except:
        #     # month = self['month']
        # return month_converter(month, 'reverse')

    def get_cost(self, obj):
            return obj['cost']

class WeeklyProfitSerializer(serializers.ModelSerializer):

    total_purchase = serializers.SerializerMethodField()
    total_profit = serializers.SerializerMethodField()
    total_weight = serializers.SerializerMethodField()

    class Meta:
        model = DailyProfitView

        fields = ('id', 'week', 'total_purchase', 'total_profit', 'total_weight')
        extra_kwargs = {'id': {'read_only': True}
                        }
    def get_total_purchase(self, obj):
        return obj['total_purchase']

    def get_total_profit(self, obj):
        return obj['total_profit']

    def get_total_weight(self, obj):
        return obj['total_weight']

class DaillyProfitSerializer(serializers.ModelSerializer):

    total_purchase = serializers.SerializerMethodField()
    total_profit = serializers.SerializerMethodField()
    total_weight = serializers.SerializerMethodField()
    day = serializers.SerializerMethodField()

    class Meta:
        model = DailyProfitView

        fields = ('id', 'date', 'day', 'total_purchase', 'total_profit', 'total_weight')
        extra_kwargs = {'id': {'read_only': True}
                        }

    def get_total_purchase(self, obj):
        return obj['total_purchase']

    def get_total_profit(self, obj):
        return obj['total_profit']

    def get_total_weight(self, obj):
        return obj['total_weight']

    def get_day(self, obj):
        return day_converter(obj['dow'])

class GraphOneSerializer(serializers.ModelSerializer):
    month = serializers.SerializerMethodField()
    percentage = serializers.SerializerMethodField()

    class Meta:
        model = MonthlyProfitView
        fields = ('id', 'month','percentage')
        extra_kwargs = {'id': {'read_only': True}
                        }

    def get_month(self, obj):
        # month = obj.month
        # return month_converter(month, 'reverse')
        return obj['month']

    def get_percentage(self, obj):
        return obj['percentage']

        # nett_weight = obj.nett_weight
        # transactions = Transaction.objects.filter(factory_id=obj.factory_id, status=Transaction.SUCCESS, date__month=obj.month)
        # gross_weight = 0
        # try:
        #     for transaction in transactions:
        #         gross_weight += transaction.weight
        #
        #     percentage = nett_weight/gross_weight
        # except ZeroDivisionError:
        #     raise APIException("Eror in Division")

class GraphTwoSerializer(serializers.ModelSerializer):
    percentage = serializers.SerializerMethodField()
    product = serializers.SerializerMethodField()

    class Meta:
        model = MonthlyProfitProductView
        # fields = '__all__'
        fields = ('month', 'year', 'factory_id', 'nett_weight', 'product', 'percentage')

    def get_percentage(self, obj):
        return obj['percentage']

    def get_product(self, obj):
        return obj['product']