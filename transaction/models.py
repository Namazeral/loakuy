from django.db import models
from user.models import User
from factory.models import Factory
from product.models import Product
# from status.models import Status

# Create your models here.

class Transaction(models.Model):
    class Meta:
        db_table = "transaction"

    KG = 'KG'
    TON = 'TON'

    #NEW = Collector create transaction
    #PENDING = one of Operator from factory pick up transaction
    #SUCCESS = transaction is clear by operator only
    #           -Fase 1= operator create
    #           -Fase 2 = operator update
    #Canceled = operator/collector cancel the transaction
    NEW = 'NEW'
    PENDING = 'PENDING'
    SUCCESS = 'SUCCESS'
    CANCELED = 'CANCELED'
    STATUS_CHOICES = (
        (NEW, 'New'),
        (PENDING, 'Pending'),
        (SUCCESS, 'Success'),
        (CANCELED, 'Canceled')
    )

    product_id = models.ForeignKey(Product, db_column='product_id', related_name='%(class)s_product_id',
                                   on_delete=models.PROTECT, blank=True, null=True)
    collector_id = models.ForeignKey(User, db_column='collector_id', related_name='%(class)s_collector_id', on_delete=models.PROTECT, blank=True, null=True)
    operator_id = models.ForeignKey(User, db_column='operator_id', related_name='%(class)s_operator_id', on_delete=models.PROTECT, blank=True, null=True)
    factory_id = models.ForeignKey(Factory, db_column='factory_id', related_name='%(class)s_factory_id', on_delete=models.PROTECT, blank=True, null=True)
    weight = models.DecimalField(max_digits=15, decimal_places=2, default=0, blank=True, null=True)
    notes_collector = models.TextField(blank=True, null=True)
    notes_operator = models.TextField(blank=True, null=True)
    cost = models.DecimalField(max_digits=15, decimal_places=2, default=0, blank=True, null=True)
    status = models.CharField(max_length=50, choices=STATUS_CHOICES, default=NEW)
    time = models.TimeField(blank=True, null=True)
    date = models.DateField(blank=True, null=True)
    code = models.CharField(max_length=50, blank=True, null=True)
    created_by = models.ForeignKey("user.User", blank=True, null=True, db_column='created_by',related_name='%(class)s_created_by', on_delete=models.PROTECT)
    created_date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    modified_by = models.ForeignKey("user.User", blank=True, null=True, db_column='modified_by',related_name='%(class)s_modified_by', on_delete=models.PROTECT)
    modified_date = models.DateTimeField(auto_now=True, blank=True, null=True)

class Profit(models.Model):
    class Meta:
        db_table = "profit"

    SUCCESS = 'SUCCESS'
    CANCELED = 'CANCELED'
    STATUS_CHOICES = (
        (SUCCESS, 'Success'),
        (CANCELED, 'Canceled')
    )


    nett_weight = models.DecimalField(max_digits=15, decimal_places=2, default=0, blank=True, null=True)
    profit = models.DecimalField(max_digits=15, decimal_places=2, default=0, blank=True, null=True)
    operator_id = models.ForeignKey(User, db_column='operator_id', related_name='%(class)s_operator_id',
                                    on_delete=models.PROTECT)
    product_id = models.ForeignKey(Product, db_column='product_id', related_name='%(class)s_product_id',
                                    on_delete=models.PROTECT, blank=True, null=True)
    factory_id = models.ForeignKey(Factory, db_column='factory_id', related_name='%(class)s_factory_id',
                                    on_delete=models.PROTECT, blank=True, null=True)
    date = models.DateField(blank=True, null=True)
    price = models.DecimalField(max_digits=15, decimal_places=2, default=0, blank=True, null=True)
    code = models.CharField(max_length=50, blank=True, null=True)
    status = models.CharField(max_length=50, choices=STATUS_CHOICES, default=SUCCESS)
    note = models.TextField(blank=True, null=True)
    created_by = models.ForeignKey("user.User", blank=True, null=True, db_column='created_by',
                                   related_name='%(class)s_created_by', on_delete=models.PROTECT)
    created_date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    modified_by = models.ForeignKey("user.User", blank=True, null=True, db_column='modified_by',
                                    related_name='%(class)s_modified_by', on_delete=models.PROTECT)
    modified_date = models.DateTimeField(auto_now=True, blank=True, null=True)

class YearlyProfitView(models.Model):
    class Meta:
        managed = False
        db_table = 'yearly_profit_view'

    id = models.IntegerField(primary_key=True)
    year = models.IntegerField()
    factory_id = models.ForeignKey(Factory, db_column='factory_id', related_name='%(class)s_factory_id', on_delete=models.PROTECT, blank=True, null=True)
    total_purchase = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    total_profit = models.DecimalField(max_digits=15, decimal_places=2, default=0)

    def __str__(self):
        return '%s' % (self.id)
    #View Design
    # CREATE VIEW yearly_profit_view AS
    # select
    #     MIN("id") as "id",
    #     extract(year from date_trunc('year', date)) as "year",
    #     factory_id,
    #     sum(price) as total_purchase,
    #     sum(profit)as total_profit
    #     from profit
    #     group by "year", profit.factory_id;

class YearlyTransactionView(models.Model):
    class Meta:
        managed = False
        db_table = 'yearly_transaction_view'

    id = models.IntegerField(primary_key=True)
    year = models.IntegerField()
    factory_id = models.ForeignKey(Factory, db_column='factory_id', related_name='%(class)s_factory_id', on_delete=models.PROTECT, blank=True, null=True)
    total_cost = models.DecimalField(max_digits=15, decimal_places=2, default=0)

    def __str__(self):
        return '%s' % (self.id)

class YearlyProfitProductView(models.Model):
    class Meta:
        managed = False
        db_table = 'yearly_profit_product_view'

    id = models.IntegerField(primary_key=True)
    year = models.IntegerField()
    factory_id = models.ForeignKey(Factory, db_column='factory_id', related_name='%(class)s_factory_id',
                                   on_delete=models.PROTECT, blank=True, null=True)
    product_id = models.ForeignKey(Product, db_column='product_id', related_name='%(class)s_product_id',
                                   on_delete=models.PROTECT, blank=True, null=True)
    total_purchase = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    total_profit = models.DecimalField(max_digits=15, decimal_places=2, default=0)

    def __str__(self):
        return '%s' % (self.id)
    #View Design
    # CREATE VIEW yearly_profit_product_view AS
    # select
    #     MIN("id") as "id",
    #     extract(year from date_trunc('year', date)) as "year",
    #     factory_id,
    #     product_id,
    #     sum(price) as total_purchase,
    #     sum(profit)as total_profit
    #     from profit
    #     group by "year", profit.factory_id, profit.product_id;

class YearlyTransactionProductView(models.Model):
    class Meta:
        managed = False
        db_table = 'yearly_transaction_product_view'

    id = models.IntegerField(primary_key=True)
    year = models.IntegerField()
    factory_id = models.ForeignKey(Factory, db_column='factory_id', related_name='%(class)s_factory_id',
                                   on_delete=models.PROTECT, blank=True, null=True)
    product_id = models.ForeignKey(Product, db_column='product_id', related_name='%(class)s_product_id',
                                   on_delete=models.PROTECT, blank=True, null=True)
    total_cost = models.DecimalField(max_digits=15, decimal_places=2, default=0)

    def __str__(self):
        return '%s' % (self.id)

class MonthlyProfitView(models.Model):
    class Meta:
        managed = False
        db_table = 'monthly_profit_view'

    id = models.IntegerField(primary_key=True)
    month = models.IntegerField(default=0)
    year = models.IntegerField()
    factory_id = models.ForeignKey(Factory, db_column='factory_id', related_name='%(class)s_factory_id',
                                   on_delete=models.PROTECT, blank=True, null=True)
    nett_weight = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    purchase = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    profit = models.DecimalField(max_digits=15, decimal_places=2, default=0)

    def __str__(self):
        return '%s' % (self.id)
    # View Design
    # CREATE VIEW monthly_profit_view AS
    # select
    #     MIN("id") as "id",
    #     extract(month from date_trunc('month', date)) as "month",
    #     extract(year from date_trunc('year', date)) as "year",
    #     factory_id,
    #     sum(nett_weight) as nett_weight,
    #     sum(price) as purchase,
    #     sum(profit) as profit
    #     from profit
    #     group by "year", "month", profit.factory_id;

class MonthlyTransactionView(models.Model):
    class Meta:
        managed = False
        db_table = 'monthly_transaction_view'

    id = models.IntegerField(primary_key=True)
    month = models.IntegerField(default=0)
    year = models.IntegerField()
    factory_id = models.ForeignKey(Factory, db_column='factory_id', related_name='%(class)s_factory_id',
                                   on_delete=models.PROTECT, blank=True, null=True)
    gross_weight = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    cost = models.DecimalField(max_digits=15, decimal_places=2, default=0)

    def __str__(self):
        return '%s' % (self.id)

class MonthlyProfitProductView(models.Model):
    class Meta:
        managed = False
        db_table = 'monthly_profit_product_view'

    id = models.IntegerField(primary_key=True)
    month = models.IntegerField(default=0)
    year = models.IntegerField()
    factory_id = models.ForeignKey(Factory, db_column='factory_id', related_name='%(class)s_factory_id',
                                   on_delete=models.PROTECT, blank=True, null=True)
    product_id = models.ForeignKey(Product, db_column='product_id', related_name='%(class)s_product_id',
                                   on_delete=models.PROTECT, blank=True, null=True)
    nett_weight = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    purchase = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    profit = models.DecimalField(max_digits=15, decimal_places=2, default=0)

    def __str__(self):
        return '%s' % (self.id)

    # View Design
    # CREATE VIEW monthly_profit_product_view AS
    # select
    #     MIN("id") as "id",
    #     extract(month from date_trunc('month', date)) as "month",
    #     extract(year from date_trunc('year', date)) as "year",
    #     factory_id,
    #     product_id,
    #     sum(nett_weight) as nett_weight,
    #     sum(price) as purchase,
    #     sum(profit) as profit
    #     from profit
    #     group by "year", "month", profit.factory_id, profit.product_id;

class MonthlyTransactionProductView(models.Model):
    class Meta:
        managed = False
        db_table = 'monthly_transaction_product_view'

    id = models.IntegerField(primary_key=True)
    month = models.IntegerField(default=0)
    year = models.IntegerField()
    factory_id = models.ForeignKey(Factory, db_column='factory_id', related_name='%(class)s_factory_id',
                                   on_delete=models.PROTECT, blank=True, null=True)
    product_id = models.ForeignKey(Product, db_column='product_id', related_name='%(class)s_product_id',
                                   on_delete=models.PROTECT, blank=True, null=True)
    gross_weight = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    cost = models.DecimalField(max_digits=15, decimal_places=2, default=0)

    def __str__(self):
        return '%s' % (self.id)

class DailyProfitView(models.Model):
    class Meta:
        managed = False
        db_table = 'daily_profit_view'

    id = models.IntegerField(primary_key=True)
    dow = models.IntegerField(default=0)
    week = models.IntegerField(default=0)
    month = models.IntegerField(default=0)
    year = models.IntegerField()
    date = models.DateField()
    factory_id = models.ForeignKey(Factory, db_column='factory_id', related_name='%(class)s_factory_id',
                                   on_delete=models.PROTECT, blank=True, null=True)
    nett_weight = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    purchase = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    profit = models.DecimalField(max_digits=15, decimal_places=2, default=0)

    def __str__(self):
        return '%s' % (self.id)

    # View Design
    # CREATE VIEW daily_profit_view AS
    # select
    #     MIN("id") as "id",
    #     date_part('dow', "date") as "dow",
    #     extract('day' from date_trunc('week', date) - date_trunc('week', date_trunc('month', date))) / 7 + 1 as "week",
    #     extract(month from date_trunc('month', date)) as "month",
    #     extract(year from date_trunc('year', date)) as "year",
    #     "date",
    #     factory_id,
    #     sum(nett_weight) as nett_weight,
    #     sum(price) as purchase,
    #     sum(profit) as profit
    #     from profit
    #     group by "week", "dow", "month", "year", "date", profit.factory_id;

class DailyTransactionView(models.Model):
    class Meta:
        managed = False
        db_table = 'daily_transaction_view'

    id = models.IntegerField(primary_key=True)
    dow = models.IntegerField(default=0)
    week = models.IntegerField(default=0)
    month = models.IntegerField(default=0)
    year = models.IntegerField()
    date = models.DateField()
    factory_id = models.ForeignKey(Factory, db_column='factory_id', related_name='%(class)s_factory_id',
                                   on_delete=models.PROTECT, blank=True, null=True)
    gross_weight = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    cost = models.DecimalField(max_digits=15, decimal_places=2, default=0)

    def __str__(self):
        return '%s' % (self.id)

class DailyProfitProductView(models.Model):
    class Meta:
        managed = False
        db_table = 'daily_profit_product_view'

    id = models.IntegerField(primary_key=True)
    dow = models.IntegerField(default=0)
    week = models.IntegerField(default=0)
    month = models.IntegerField(default=0)
    year = models.IntegerField()
    date = models.DateField()
    factory_id = models.ForeignKey(Factory, db_column='factory_id', related_name='%(class)s_factory_id',
                                   on_delete=models.PROTECT, blank=True, null=True)
    product_id = models.ForeignKey(Product, db_column='product_id', related_name='%(class)s_product_id',
                                   on_delete=models.PROTECT, blank=True, null=True)
    nett_weight = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    purchase = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    profit = models.DecimalField(max_digits=15, decimal_places=2, default=0)

    def __str__(self):
        return '%s' % (self.id)

    # View Design
    # CREATE VIEW daily_profit_product_view AS
    # select
    #     MIN("id") as "id",
    #     date_part('dow', "date") as "dow",
    #     extract('day' from date_trunc('week', date) - date_trunc('week', date_trunc('month', date))) / 7 + 1 as "week",
    #     extract(month from date_trunc('month', date)) as "month",
    #     extract(year from date_trunc('year', date)) as "year",
    #     "date",
    #     factory_id,
    #     product_id,
    #     sum(nett_weight) as nett_weight,
    #     sum(price) as purchase,
    #     sum(profit) as profit
    #     from profit
    #     group by "week", "dow", "month", "year", "date", profit.factory_id, profit.product_id;
class DailyTransactionProductView(models.Model):
    class Meta:
        managed = False
        db_table = 'daily_transaction_product_view'

    id = models.IntegerField(primary_key=True)
    dow = models.IntegerField(default=0)
    week = models.IntegerField(default=0)
    month = models.IntegerField(default=0)
    year = models.IntegerField()
    date = models.DateField()
    factory_id = models.ForeignKey(Factory, db_column='factory_id', related_name='%(class)s_factory_id',
                                   on_delete=models.PROTECT, blank=True, null=True)
    product_id = models.ForeignKey(Product, db_column='product_id', related_name='%(class)s_product_id',
                                   on_delete=models.PROTECT, blank=True, null=True)
    gross_weight = models.DecimalField(max_digits=15, decimal_places=2, default=0)
    cost = models.DecimalField(max_digits=15, decimal_places=2, default=0)

    def __str__(self):
        return '%s' % (self.id)