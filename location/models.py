from django.db import models

class City(models.Model):
    class Meta:
        db_table = 'city'

    ACTIVE = 'ACTIVE'
    NOT_ACTIVE = 'NOT_ACTIVE'
    STATUS_CHOICES = (
        (ACTIVE, 'Active'),
        (NOT_ACTIVE, 'Not Active'),
    )

    name = models.CharField(max_length=100, blank=True, null=True)
    status = models.CharField(max_length=50, choices=STATUS_CHOICES, default=ACTIVE)
    latitude = models.FloatField(default=0.0)
    longitude = models.FloatField(default=0.0)
    created_by = models.ForeignKey("user.User", blank=True, null=True, db_column='created_by', related_name='%(class)s_created_by', on_delete=models.PROTECT)
    created_date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    modified_by = models.ForeignKey("user.User", blank=True, null=True, db_column='modified_by', related_name='%(class)s_modified_by', on_delete=models.PROTECT)
    modified_date = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return '%s' % (self.id)

class District(models.Model):
    class Meta:
        db_table = 'district'

    ACTIVE = 'ACTIVE'
    NOT_ACTIVE = 'NOT_ACTIVE'
    STATUS_CHOICES = (
        (ACTIVE, 'Active'),
        (NOT_ACTIVE, 'Not Active'),
    )

    city_id = models.ForeignKey(City, db_column='city_id', related_name='%(class)s_city_id', on_delete=models.PROTECT)
    name = models.CharField(max_length=100, blank=True, null=True)
    status = models.CharField(max_length=50, choices=STATUS_CHOICES, default=ACTIVE)
    latitude = models.FloatField(default=0.0)
    longitude = models.FloatField(default=0.0)
    created_by = models.ForeignKey("user.User", blank=True, null=True, db_column='created_by', related_name='%(class)s_created_by', on_delete=models.PROTECT)
    created_date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    modified_by = models.ForeignKey("user.User", blank=True, null=True, db_column='modified_by', related_name='%(class)s_modified_by', on_delete=models.PROTECT)
    modified_date = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return '%s' % (self.id)

