from django.urls import path, include
from rest_framework import routers
from user import views
from rest_framework_jwt.views import obtain_jwt_token
from rest_framework_jwt.views import refresh_jwt_token

router = routers.DefaultRouter()
router.register(r'manager', views.ManagerViewSet)
router.register(r'operator', views.OperatorViewSet)
router.register(r'get-user', views.GetUserViewsSet)
router.register(r'change-password', views.ChangePasswordViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('auth/', include('rest_auth.urls')),
    path('get-token/', obtain_jwt_token),
    path('get-token/admin/', views.AdminLogInView.as_view()),
    path('get-token/manager/', views.ManagerLogInView.as_view()),
    path('get-token/operator/', views.OperatorLogInView.as_view()),
    path('get-token-refresh/', refresh_jwt_token),
]