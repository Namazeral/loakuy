from rest_framework import permissions
from user.models import User
from django.contrib import auth


class AdminOnly(permissions.BasePermission):
    meta = {
        "meta": {
            "code": 401,
            "status": False,
            "message": '',
        }
    }

    data = {
        "data": 'You do not have permission to perform this action.'
    }

    message = dict(meta, **data)

    def has_permission(self, request, view):
        # user = auth.get_user(request)
        user = request.user
        return bool(user.type == user.ADMIN)

    def has_object_permission(self, request, view, obj):
        # user = auth.get_user(request)
        user = request.user
        return bool(user.type == user.ADMIN)

class ManagerOnly(permissions.BasePermission):
    meta = {
        "meta": {
            "code": 401,
            "status": False,
            "message": '',
        }
    }

    data = {
        "data": 'You do not have permission to perform this action.'
    }

    message = dict(meta, **data)

    def has_permission(self, request, view):
        # user = auth.get_user(request)
        user = request.user
        return bool(user.type == user.MANAGER)

    def has_object_permission(self, request, view, obj):
        # user = auth.get_user(request)
        user = request.user
        return bool(user.type == user.MANAGER)

class RegisterOnly(permissions.BasePermission):
    meta = {
        "meta": {
            "code": 401,
            "status": False,
            "message": '',
        }
    }

    data = {
        "data": 'You do not have permission to perform this action.'
    }

    message = dict(meta, **data)

    def has_permission(self, request, view):
        # user = auth.get_user(request)
        user = request.user
        return bool(user.type == user.MANAGER)

class CollectorOnly(permissions.BasePermission):
    meta = {
        "meta": {
            "code": 401,
            "status": False,
            "message": '',
        }
    }

    data = {
        "data": 'You do not have permission to perform this action.'
    }

    message = dict(meta, **data)

    def has_permission(self, request, view):
        # user = auth.get_user(request)
        user = request.user
        return bool(user.type == user.COLLECTOR)

    def has_object_permission(self, request, view, obj):
        # user = auth.get_user(request)
        user = request.user
        return bool(user.type == user.type == User.COLLECTOR)

class OperatorOnly(permissions.BasePermission):
    meta = {
        "meta": {
            "code": 401,
            "status": False,
            "message": '',
        }
    }

    data = {
        "data": 'You do not have permission to perform this action.'
    }

    message = dict(meta, **data)

    def has_permission(self, request, view):
        # user = auth.get_user(request)
        user = request.user
        return bool(user.type == user.OPERATOR)

    def has_object_permission(self, request, view, obj):
        # user = auth.get_user(request)
        user = request.user
        return bool(user.type == user.OPERATOR)

class CollectorOperatorOnly(permissions.BasePermission):
    meta = {
        "meta": {
            "code": 401,
            "status": False,
            "message": '',
        }
    }

    data = {
        "data": 'You do not have permission to perform this action.'
    }

    message = dict(meta, **data)

    def has_permission(self, request, view):
        # user = auth.get_user(request)
        user = request.user
        return bool(user.type == user.COLLECTOR or user.type == user.OPERATOR)

    def has_object_permission(self, request, view, obj):
        # user = auth.get_user(request)
        user = request.user
        return bool(user.type == user.COLLECTOR or user.type == user.OPERATOR)

class ManagerOperatorOnly(permissions.BasePermission):
    meta = {
        "meta": {
            "code": 401,
            "status": False,
            "message": '',
        }
    }

    data = {
        "data": 'You do not have permission to perform this action.'
    }

    message = dict(meta, **data)

    def has_permission(self, request, view):
        # user = auth.get_user(request)
        user = request.user
        return bool(user.type == user.MANAGER or user.type == user.OPERATOR)

    def has_object_permission(self, request, view, obj):
        # user = auth.get_user(request)
        user = request.user
        return bool(user.type == user.MANAGER or user.type == user.OPERATOR)

class AdminManagerOnly(permissions.BasePermission):
    meta = {
        "meta": {
            "code": 401,
            "status": False,
            "message": '',
        }
    }

    data = {
        "data": 'You do not have permission to perform this action.'
    }

    message = dict(meta, **data)

    def has_permission(self, request, view):
        # user = auth.get_user(request)
        user = request.user
        return bool(user.type == user.MANAGER or user.type == user.ADMIN)

    def has_object_permission(self, request, view, obj):
        # user = auth.get_user(request)
        user = request.user
        return bool(user.type == user.MANAGER or user.type == user.ADMIN)