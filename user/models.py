from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.contrib.postgres.fields import CICharField

from factory.models import Factory
from location.models import City, District

# Create your models here.

class User (AbstractUser):

    class Meta:
        db_table = 'user'
        unique_together = (
            ("email", "mobile_number", "type"),
        )

    ADMIN = 'ADMIN'
    OPERATOR = 'OPERATOR'
    MANAGER = "MANAGER"
    COLLECTOR = "COLLECTOR"
    CUSTOMER = "CUSTOMER"
    REGISTER = "REGISTER"
    ROLE_CHOICES = (
        (ADMIN, 'Admin'),
        (OPERATOR, 'Operator'),
        (MANAGER, 'Manager'),
        (COLLECTOR, "Collector"),
        (CUSTOMER, "Customer"),
        (REGISTER, "Register")
    )

    NEW = 'NEW'
    ACTIVE = 'ACTIVE'
    NOT_ACTIVE = 'NOT_ACTIVE'
    STATUS_CHOICES = (
        (NEW, 'New'),
        (ACTIVE, 'Active'),
        (NOT_ACTIVE, 'Not Active'),
    )


    username = models.CharField(max_length=50,blank=True, null=True)
    email = models.EmailField(_('email address'), unique=True)
    type = models.CharField(max_length=50, choices=ROLE_CHOICES)
    mobile_number = CICharField(max_length=100, blank=True, null=True, error_messages={'unique': "Member with this mobile number already exists. Please try another one."})
    status = models.CharField(max_length=50, choices=STATUS_CHOICES, default=NEW)
    factory_id = models.ForeignKey(Factory, blank=True, null=True, db_column='factory_id', related_name='%(class)s_factory_id', on_delete=models.PROTECT)
    id_os = models.CharField(max_length=50, blank=True, null=True)
    address = models.CharField(max_length=50, blank=True, null=True)
    city = models.ForeignKey(City, blank=True, null=True, db_column='city_id', related_name='%(class)s_city_id', on_delete=models.PROTECT)
    district = models.ForeignKey(District, blank=True, null=True, db_column='district_id', related_name='%(class)s_district_id', on_delete=models.PROTECT)
    longitude = models.CharField(max_length=100, blank=True, null=True)
    latitude = models.CharField(max_length=100, blank=True, null=True)
    modified_by = models.ForeignKey("user.User", blank=True, null=True, db_column='modified_by', related_name='%(class)s_modified_by', on_delete=models.PROTECT)
    modified_date = models.DateTimeField(auto_now=True, blank=True, null=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'first_name', 'last_name', 'type']

    def __str__(self):
        return "{}".format(self.email)

# class UserProfile(models.Model):
#
#     class Meta:
#         db_table = 'user_profile'
#
#     NEW = 'NEW'
#     ACTIVE = 'ACTIVE'
#     NOT_ACTIVE = 'NOT_ACTIVE'
#     STATUS_CHOICES = (
#         (NEW, 'New'),
#         (ACTIVE, 'Active'),
#         (NOT_ACTIVE, 'Not Active'),
#     )
#     user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, db_column='user_id', related_name='%(class)s_user_id')
#     type = models.CharField(max_length=50, choices=STATUS_CHOICES)
#     factory_id = models.ForeignKey(Factory, blank=True, null=True, db_column='factory_id', related_name='%(class)s_factory_id', on_delete=models.PROTECT)
#     id_os = models.CharField(max_length=50, blank=True, null=True)
#     address = models.CharField(max_length=50, blank=True, null=True)
#     city = models.ForeignKey(City, blank=True, null=True, db_column='city_id', related_name='%(class)s_city_id', on_delete=models.PROTECT)
#     district = models.ForeignKey(District, blank=True, null=True, db_column='district_id', related_name='%(class)s_district_id', on_delete=models.PROTECT)
#     last_login = models.DateTimeField(blank=True, null=True)
#     date_registered = models.DateTimeField(blank=True, null=True)
#     longitude = models.CharField(max_length=100, blank=True, null=True)
#     latitude = models.CharField(max_length=100, blank=True, null=True)
#     created_by = models.ForeignKey("user.User", blank=True, null=True, db_column='created_by',related_name='%(class)s_created_by', on_delete=models.PROTECT)
#     created_date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
#     modified_by = models.ForeignKey("user.User", blank=True, null=True, db_column='modified_by',related_name='%(class)s_modified_by', on_delete=models.PROTECT)
#     modified_date = models.DateTimeField(auto_now=True, blank=True, null=True)
#
#     def __str__(self):
#         return '%s' % (self.id)

