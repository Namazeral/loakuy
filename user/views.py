from django.conf import settings
from django.utils.crypto import get_random_string
from django.views import View
from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework import viewsets, mixins
from rest_framework.generics import GenericAPIView
from rest_framework_jwt.views import BaseJSONWebTokenAPIView, JSONWebTokenSerializer
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework_jwt.serializers import \
    JSONWebTokenSerializer, RefreshAuthTokenSerializer, \
    VerifyAuthTokenSerializer, ImpersonateAuthTokenSerializer
from rest_framework_jwt.compat import set_cookie_with_token
from rest_framework_jwt.settings import api_settings
from rest_framework.decorators import action

from config.helper import MyResponse

from user.models import User
from user.serializers import ManagerOperatorSerializer, UserDataSerializer, ChangePasswordSerializer, OperatorDeleteSerializer
from user import permissions
from factory.models import Factory


class ManagerViewSet(viewsets.ModelViewSet):
    queryset = User.objects.filter(type='MANAGER', is_active=True)
    serializer_class = ManagerOperatorSerializer

    def get_permissions(self):
        permission_classes = []
        if self.action == 'create':
            permission_classes = [permissions.AdminOnly]
        elif self.action == 'retrieve' or self.action == 'update' or self.action == 'partial_update':
            permission_classes = [permissions.ManagerOnly]
        elif self.action == 'list' or self.action == 'destroy':
            permission_classes = [permissions.AdminOnly]
        return [permission() for permission in permission_classes]

    def create(self, request, *args, **kwargs):

        request.data['type'] = User.MANAGER

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        data = {"detail": serializer.data}

        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                          "Manager berhasil didaftarkan.", data)

    def retrieve(self, request, pk=None):
        # user = auth.get_user(request)
        user = request.user
        queryset = User.objects.filter(type='MANAGER', is_active=True, factory_id=user.factory_id)
        detail = get_object_or_404(queryset, pk=pk)

        if str(user) != str(detail):
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "USER IS NOT MATCHED", "")

        serializer = self.get_serializer(detail)
        response_data = {"detail": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                          "", response_data)

class OperatorViewSet(viewsets.ModelViewSet):
    queryset = User.objects.filter(type='OPERATOR', is_active=True)
    serializer_class = ManagerOperatorSerializer

    def get_permissions(self):
        permission_classes = []
        if self.action == 'create':
            permission_classes = [permissions.ManagerOnly]
        elif self.action == 'retrieve' or self.action == 'update' or self.action == 'partial_update':
            permission_classes = [permissions.ManagerOperatorOnly]
        elif self.action == 'list' or self.action == 'deleteoperator':
            permission_classes = [permissions.ManagerOnly]
        elif self.action == 'destroy':
            permission_classes = [permissions.ManagerOnly]
        return [permission() for permission in permission_classes]

    def create(self, request, *args, **kwargs):
        try:
            # user = auth.get_user(request)
            user = request.user

            request.data['type'] = User.OPERATOR
            request.data['factory_id'] = int(user.factory_id.id)

            #check phone number
            phone_number = request.data["mobile_number"]
            if phone_number[2] == '0':
                request.data["mobile_number"] = '62' + str(phone_number[3:])

            # request.data['factory_id'] =

            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            data = {"detail": serializer.data}

            return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                              "Operator berhasil didaftarkan.", data)
        except Exception as e:
            if str(e) == "{'email': [ErrorDetail(string='user with this email address already exists.', code='unique')]}":
                return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                                  'Tidak dapat mendaftarkan operator, email sudah terdaftar', '')
            else:
                return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                                  str(e), '')

    def update(self, request, pk=None):
        # user = auth.get_user(request)
        user = request.user

        if user.type == user.MANAGER or str(user.id) == str(pk):
            queryset = User.objects.filter(type='OPERATOR', is_active=True, factory_id=user.factory_id)
            detail = get_object_or_404(queryset, pk=pk)

            if user.factory_id != detail.factory_id:
                return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                                  "THERE'S NO MATCHED OPERATOR FROM YOUR FACTORY", "")

            request.data['type'] = detail.type
            request.data['email'] = detail.email
            # request.data['mobile_number'] = detail.mobile_number
            request.data['password'] = detail.password

            # check phone number
            phone_number = request.data["mobile_number"]
            if phone_number[2] == '0':
                request.data["mobile_number"] = '62' + str(phone_number[3:])

            serializer = self.get_serializer(detail, data=request.data)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            response_data = {"detail": serializer.data}
            return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                              "Operator berhasil diperbarui", response_data)

        else:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "Anda tidak memiliki izin", "")

    def retrieve(self, request, pk=None):
        # user = auth.get_user(request)
        user = request.user

        if user.type == user.MANAGER:
            queryset = User.objects.filter(type='OPERATOR', is_active=True, factory_id=user.factory_id)
            detail = get_object_or_404(queryset, pk=pk)

            if user.factory_id != detail.factory_id:
                return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                                  "THERE'S NO MATCHED OPERATOR FROM YOUR FACTORY", "")

            serializer = self.get_serializer(detail)
            response_data = {"detail": serializer.data}
            return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                              "", response_data)

        elif user.type == user.OPERATOR:
            queryset = self.get_queryset()
            detail = get_object_or_404(queryset, pk=pk)
            if str(user) != str(detail):
                return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                                  "USER IS NOT MATCHED", "")
            serializer = self.get_serializer(detail)
            response_data = {"detail": serializer.data}
            return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                              "", response_data)

    def list(self, request):
        # user = auth.get_user(request)
        user = request.user

        if user.type == user.MANAGER:
            queryset = User.objects.filter(type='OPERATOR', is_active=True, factory_id=user.factory_id).exclude(status=User.NOT_ACTIVE)

            serializer = self.get_serializer(queryset, many=True)
            response_data = {"detail": serializer.data}
            return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                              "", response_data)

    @action(detail=True, methods=['post'])
    def deleteoperator(self, request, pk=None):
        user = request.user
        request.data['status'] = User.NOT_ACTIVE

        queryset = self.get_queryset()
        detail = get_object_or_404(queryset, pk=pk)
        serializer = OperatorDeleteSerializer(detail, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(modified_by_id=user.id)

        data = {"detail": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                          "Operator berhasil di hapus.", data)

class GetUserViewsSet(mixins.RetrieveModelMixin,
                   viewsets.GenericViewSet):
    queryset = User.objects.filter(is_active=True)
    serializer_class = UserDataSerializer

    def get_permissions(self):
        permission_classes = []
        if self.action == 'list':
            permission_classes = [AllowAny]
        return [permission() for permission in permission_classes]

    def list(self, request):
        # user = auth.get_user(request)
        user = request.user
        pk = user.id
        queryset = User.objects.filter(id=pk).first()

        if str(user) != str(queryset):
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "USER IS NOT MATCHED", "")

        serializer = self.get_serializer(queryset)
        response_data = {"list": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                          "", response_data)

class ChangePasswordViewSet(mixins.UpdateModelMixin,
                            viewsets.GenericViewSet):
    queryset = User.objects.filter(is_active=True)
    serializer_class = ChangePasswordSerializer

    def update(self, request, pk=None):
        # user = auth.get_user(request)
        user = request.user

        if str(user.id) == str(pk):
            queryset = User.objects.filter(is_active=True, factory_id=user.factory_id)
            detail = get_object_or_404(queryset, pk=user.id)

            detail.set_password(request.data['password'])
            detail.save()
            serializer = self.get_serializer(detail, data=request.data)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            response_data = {"detail": serializer.data}
            return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                              "Password berhasil diperbarui", response_data)

        else:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "Anda tidak memiliki izin untuk user", "")

class AdminLogInView(GenericAPIView):
    permission_classes = ()
    authentication_classes = ()

    serializer_class = JSONWebTokenSerializer

    def post(self, request, *args, **kwargs):
        try:
            serializer = self.get_serializer(data=request.data)

            serializer.is_valid(raise_exception=True)

            user = serializer.validated_data.get('user') or request.user
            if user.type == User.ADMIN:
                token = serializer.validated_data.get('token')
                issued_at = serializer.validated_data.get('issued_at')
                response_data = JSONWebTokenAuthentication. \
                    jwt_create_response_payload(token, user, request, issued_at)

                response = Response(response_data)

                if api_settings.JWT_AUTH_COOKIE:
                    set_cookie_with_token(response, api_settings.JWT_AUTH_COOKIE, token)

                return response

            else:
                return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                                  "Harap log in sebagai admin, user bukan admin.", "")
        except:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "Email atau password salah.", "")

class ManagerLogInView(GenericAPIView):
    permission_classes = ()
    authentication_classes = ()

    serializer_class = JSONWebTokenSerializer

    def post(self, request, *args, **kwargs):
        try:
            serializer = self.get_serializer(data=request.data)

            serializer.is_valid(raise_exception=True)

            user = serializer.validated_data.get('user') or request.user
            if user.type == User.MANAGER:
                token = serializer.validated_data.get('token')
                issued_at = serializer.validated_data.get('issued_at')
                response_data = JSONWebTokenAuthentication. \
                    jwt_create_response_payload(token, user, request, issued_at)

                response = Response(response_data)

                if api_settings.JWT_AUTH_COOKIE:
                    set_cookie_with_token(response, api_settings.JWT_AUTH_COOKIE, token)

                return response

            else:
                return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                                  "Harap log in sebagai manager, user bukan manager.", "")
        except:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "Email atau password salah.", "")

class OperatorLogInView(GenericAPIView):
    permission_classes = ()
    authentication_classes = ()

    serializer_class = JSONWebTokenSerializer

    def post(self, request, *args, **kwargs):
        try:
            serializer = self.get_serializer(data=request.data)

            serializer.is_valid(raise_exception=True)

            user = serializer.validated_data.get('user') or request.user

            if user.type == User.OPERATOR:
                if user.status == User.NOT_ACTIVE:
                    return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"],
                                      settings.RESPONSE_META["FAILED"]["STATUS"],
                                      "Akun anda telah di nonaktifkan.", "")

                token = serializer.validated_data.get('token')
                issued_at = serializer.validated_data.get('issued_at')
                response_data = JSONWebTokenAuthentication. \
                    jwt_create_response_payload(token, user, request, issued_at)

                response = Response(response_data)

                if api_settings.JWT_AUTH_COOKIE:
                    set_cookie_with_token(response, api_settings.JWT_AUTH_COOKIE, token)

                return response

            else:
                return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                                  "Harap log in sebagai operator, user bukan operator.", "")
        except:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "Email atau password salah.", "")