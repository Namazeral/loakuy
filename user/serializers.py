from rest_framework import serializers
from user.models import User


class UserSerializer(serializers.ModelSerializer):
    # profile = UserProfileSerializer(required=True)

    class Meta:
        model = User
        fields = ('id', 'email', 'mobile_number', 'first_name', 'last_name', 'password', 'type', 'longitude', 'latitude', 'modified_by', 'modified_date')
        extra_kwargs = {'password': {'write_only': True},
                        'id': {'read_only': True}}

    def create(self, validated_data):
        password = validated_data.pop('password')
        user = User(**validated_data)
        user.set_password(password)
        user.save()
        # UserProfile.objects.create(user=user, **profile_data)
        return user

    def update(self, instance, validated_data):

        instance.email = validated_data.get('email', instance.email)
        instance.save()

        instance.last_login = validated_data.get('last_login', instance.last_login)
        instance.longitude = validated_data.get('longitude', instance.longitude)
        instance.latitude = validated_data.get('latitude', instance.latitude)
        instance.modified_by = validated_data.get('modified_by', instance.modified_by)
        instance.modified_date = validated_data.get('modified_date', instance.modified_date)
        instance.save()

        return instance

# class ManagerProfileSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = UserProfile
#         fields = ('last_login', 'date_registered', 'created_by', 'modified_by', 'created_date', 'modified_date')
#
# class FactoryProfileSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = UserProfile
#         fields = ('factory_name', 'longitude', 'latitude')

class UserDataSerializer(serializers.ModelSerializer):
    # profile = UserProfileSerializer(required=True)
    factory = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'email', 'mobile_number', 'first_name', 'last_name', 'type', 'address', 'longitude', 'latitude', 'factory')
        extra_kwargs = {'id': {'read_only': True}}

    def get_factory(self, obj):
        if obj.factory_id:
            factory = {
                'factory_id': obj.factory_id.id,
                'factory_name': obj.factory_id.name,
                'address': obj.factory_id.address,
                'longitude': obj.factory_id.longitude,
                'latitude': obj.factory_id.latitude,
            }
        else:
            factory = None
        return factory

class ManagerOperatorSerializer(serializers.ModelSerializer):
    factory = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'email', 'mobile_number', 'first_name', 'last_name', 'password', 'type', 'longitude', 'latitude', 'modified_by', 'modified_date', 'factory_id', 'factory', 'is_active')
        extra_kwargs = {'password': {'write_only': True},
                        'id': {'read_only': True}}

    def get_factory(self, obj):
        if obj.factory_id:
            factory = {
                'factory_id': obj.factory_id.id,
                'factory_name': obj.factory_id.name,
                'address': obj.factory_id.address,
                'longitude': obj.factory_id.longitude,
                'latitude': obj.factory_id.latitude,
            }
        else:
            factory = None
        return factory

    def create(self, validated_data):
        password = validated_data.pop('password')
        user = User(**validated_data)
        user.set_password(password)
        user.save()

        return user

    def update(self, instance, validated_data):
        instance.mobile_number = validated_data.get('mobile_number', instance.mobile_number)
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.longitude = validated_data.get('longitude', instance.longitude)
        instance.latitude = validated_data.get('latitude', instance.latitude)
        instance.is_active = validated_data.get('is_active', instance.is_active)
        instance.modified_by = validated_data.get('modified_by', instance.modified_by)
        instance.modified_date = validated_data.get('modified_date', instance.modified_date)
        instance.save()

        return instance

class ChangePasswordSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'email')
        extra_kwargs = {'id': {'read_only': True},
                        'email': {'read_only': True}}

class OperatorDeleteSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id','email', 'status')
        extra_kwargs = {'id': {'read_only': True},
                        'email': {'read_only': True} }

    def update(self, instance, validated_data):
        instance.status = validated_data.get('status', instance.status)
        instance.save()

        return instance