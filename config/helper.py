from rest_framework import status
from rest_framework.response import Response
from django.http import JsonResponse
import random
import string
from math import ceil

def week_of_month(dt):
    """ Returns the week of the month for the specified date.
    """

    first_day = dt.replace(day=1)

    dom = dt.day
    adjusted_dom = dom + first_day.weekday()

    return int(ceil(adjusted_dom/7.0))

def month_converter(month, converter='initial'):
    if converter == 'reverse':
        month_value = {
            1: 'Januari',
            2: 'Februari',
            3: 'Maret',
            4: 'April',
            5: 'Mei',
            6: 'Juni',
            7: 'Juli',
            8: 'Agustus',
            9: 'September',
            10: 'Oktober',
            11: 'November',
            12: 'Desember'
        }

    else:
        month_value = {
            'Januari': 1,
            'Februari': 2,
            'Maret': 3,
            'April': 4,
            'Mei': 5,
            'Juni': 6,
            'Juli': 7,
            'Agustus': 8,
            'September': 9,
            'Oktober': 10,
            'November': 11,
            'Desember': 12
        }

    return month_value[month]

def day_converter(day):
    day_value = {
        1: 'Senin',
        2: 'Selasa',
        3: 'Rabu',
        4: 'Kamis',
        5: 'Jumat',
        6: 'Sabtu',
        7: 'Minggu',
        0: 'Minggu',
    }

    return day_value[day]

def MyResponse(code=400, status=False, message="", data={}, header_status_code=status.HTTP_200_OK):
    meta = {
        "meta": {
            "code": code,
            "status": status,
            "message": message,
        }
    }

    data = {
        "data": data
    }

    res = dict(meta, **data)

    return Response(res, status=header_status_code)

def MyResponseJson(code=400, status=False, message="", data={}):
    meta = {
        "meta": {
            "code": code,
            "status": status,
            "message": message,
        }
    }

    data = {
        "data": data
    }

    res = dict(meta, **data)

    return JsonResponse(res, safe=False)

def custom_jwt_create_response_payload(token, user=None, request=None, issued_at=None):
    meta = {
        "meta": {
            "code": 200,
            "status": True,
            "message": 'Succesfully Log in',
        }
    }

    data = {
        "data": {'pk': issued_at, 'token': token}
    }

    res = dict(meta, **data)

    return res

def code_generator(type, stringLength=8):
    letters = string.ascii_uppercase
    digits = string.digits
    code = ''.join(random.choice(letters + digits) for i in range(stringLength))
    return type + code