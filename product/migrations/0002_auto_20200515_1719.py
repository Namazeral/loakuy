# Generated by Django 2.2.6 on 2020-05-15 17:19

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='code',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='product',
            name='status',
            field=models.CharField(choices=[('ACTIVE', 'Active'), ('NOT_ACTIVE', 'Not Active')], default='ACTIVE', max_length=50),
        ),
        migrations.CreateModel(
            name='ProductHistory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=100, null=True)),
                ('max_weight', models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=15, null=True)),
                ('min_weight', models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=15, null=True)),
                ('price', models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=15, null=True)),
                ('unit', models.CharField(choices=[('KG', 'Kg'), ('TON', 'TON')], default='KG', max_length=50)),
                ('order', models.IntegerField()),
                ('product_id', models.ForeignKey(blank=True, db_column='product_id', null=True, on_delete=django.db.models.deletion.PROTECT, related_name='producthistory_product_id', to='product.Product')),
            ],
            options={
                'db_table': 'product_history',
            },
        ),
    ]
