# Generated by Django 2.2.6 on 2020-01-27 06:35

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('factory', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=100, null=True)),
                ('max_weight', models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=15, null=True)),
                ('min_weight', models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=15, null=True)),
                ('price', models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=15, null=True)),
                ('unit', models.CharField(choices=[('KG', 'Kg'), ('TON', 'TON')], default='KG', max_length=50)),
                ('created_date', models.DateTimeField(auto_now_add=True, null=True)),
                ('modified_date', models.DateTimeField(auto_now=True, null=True)),
                ('created_by', models.ForeignKey(blank=True, db_column='created_by', null=True, on_delete=django.db.models.deletion.PROTECT, related_name='product_created_by', to=settings.AUTH_USER_MODEL)),
                ('factory_id', models.ForeignKey(blank=True, db_column='factory_id', null=True, on_delete=django.db.models.deletion.PROTECT, related_name='product_factory_id', to='factory.Factory')),
                ('modified_by', models.ForeignKey(blank=True, db_column='modified_by', null=True, on_delete=django.db.models.deletion.PROTECT, related_name='product_modified_by', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'product',
            },
        ),
    ]
