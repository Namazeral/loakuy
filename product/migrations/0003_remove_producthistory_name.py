# Generated by Django 2.2.6 on 2020-05-15 17:31

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0002_auto_20200515_1719'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='producthistory',
            name='name',
        ),
    ]
