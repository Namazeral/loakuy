from django.conf import settings
from rest_framework import viewsets, mixins
from rest_framework.permissions import AllowAny
from rest_framework.decorators import action
from django.shortcuts import get_object_or_404

from config.helper import MyResponse, code_generator

from product.models import Product, ProductHistory
from user import permissions
from product.serializers import ProductSerializer, ProductDeleteSerializer
from django.contrib.auth import get_user


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


    def get_permissions(self):
        permission_classes = []
        if self.action == 'partial_update' or self.action == 'destroy':
            permission_classes = [permissions.AdminOnly]
        elif self.action == 'create' or self.action == 'update' or self.action == 'deleteproduct':
            permission_classes = [permissions.ManagerOnly]
        elif self.action == 'list' or self.action == 'retrieve':
            permission_classes = [permissions.ManagerOperatorOnly]
        return [permission() for permission in permission_classes]

    def create(self, request):
        # user = get_user(request)
        user = request.user

        request.data['factory_id'] = user.factory_id.id

        code_type = 'ID-' + str(user.factory_id.id)

        identic = False
        while not identic:
            code = code_generator(code_type, stringLength=8)
            check_code = Product.objects.filter(code=code)
            identic = True if not check_code else False
        request.data['code'] = code

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by_id=user.id,
                        modified_by_id=user.id)

        data = {"detail": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                          "Produk berhasil dibuat.", data)

    def list(self, request):
        # user = get_user(request)
        user = request.user
        queryset = self.queryset
        # queryset = queryset.filter(factory_id=str(user.factory_id.id)).order_by('name', '-created_date').distinct("name")
        queryset = queryset.filter(factory_id=user.factory_id, status=Product.ACTIVE)
        serializer = self.get_serializer(queryset, many=True)
        data = {"list": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"], "",
                          data)

    def update(self, request, pk=None):
        user = request.user
        queryset = self.get_queryset()
        detail = get_object_or_404(queryset, pk=pk)

        if user.factory_id != detail.factory_id:
            return MyResponse(settings.RESPONSE_META["FAILED"]["CODE"], settings.RESPONSE_META["FAILED"]["STATUS"],
                              "THERE'S NO MATCHED PRODUCT FROM YOUR FACTORY", "")

        product_history = ProductHistory.objects.filter(product_id=detail.id).order_by('order').last()
        order = int(product_history.order) + 1 if product_history else 1

        ProductHistory.objects.create(
            max_weight=detail.max_weight,
            min_weight=detail.min_weight,
            price=detail.price,
            order=order,
            product_id=detail
        )

        serializer = self.get_serializer(detail, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(modified_by_id=user.id)

        data = {"detail": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                          "Produk berhasil diperbarui.", data)

    @action(detail=True, methods=['post'])
    def deleteproduct(self, request, pk=None):
        user = request.user
        request.data['status'] = Product.NOT_ACTIVE

        queryset = self.get_queryset()
        detail = get_object_or_404(queryset, pk=pk)
        serializer = ProductDeleteSerializer(detail, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(modified_by_id=user.id)

        data = {"detail": serializer.data}
        return MyResponse(settings.RESPONSE_META["SUCCESS"]["CODE"], settings.RESPONSE_META["SUCCESS"]["STATUS"],
                          "Produk berhasil dihapus.", data)


