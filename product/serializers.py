from rest_framework import serializers
from product.models import Product, ProductHistory

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'name','max_weight', 'min_weight', 'price', 'factory_id', 'code', 'created_by', 'modified_by',
                  'created_date', 'modified_date')
        extra_kwargs = {'id': {'read_only': True}}

    def create(self, validated_data):
        product = Product.objects.create(**validated_data)
        created_by_id = validated_data["created_by_id"]

        ProductHistory.objects.create(
            max_weight=product.max_weight,
            min_weight=product.min_weight,
            price=product.price,
            order=1,
            product_id=product
        )

        return product

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.max_weight = validated_data.get('max_weight', instance.max_weight)
        instance.min_weight = validated_data.get('min_weight', instance.min_weight)
        instance.price = validated_data.get('price', instance.price)
        instance.save()

        return instance

class ProductDeleteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('id','name','code', 'status')
        extra_kwargs = {'id': {'read_only': True}}

    def update(self, instance, validated_data):
        instance.status = validated_data.get('status', instance.status)
        instance.save()

        return instance