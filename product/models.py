from django.db import models
from factory.models import Factory


# Create your models here.
class Product(models.Model):
    class Meta:
        db_table = "product"

    KG = 'KG'
    TON = 'TON'
    UNIT_CHOICES = (
        (KG, 'Kg'),
        (TON, 'TON'),
    )

    ACTIVE = 'ACTIVE'
    NOT_ACTIVE = 'NOT_ACTIVE'
    STATUS_CHOICES = (
        (ACTIVE, 'Active'),
        (NOT_ACTIVE, 'Not Active'),
    )

    name = models.CharField(max_length=100, blank=True, null=True)
    max_weight = models.DecimalField(max_digits=15, decimal_places=2, default=0, blank=True, null=True)
    min_weight = models.DecimalField(max_digits=15, decimal_places=2, default=0, blank=True, null=True)
    price = models.DecimalField(max_digits=15, decimal_places=2, default=0, blank=True, null=True)
    code = models.CharField(max_length=50, blank=True, null=True)
    status = models.CharField(max_length=50, choices=STATUS_CHOICES, default=ACTIVE)
    # period_start = models.DateField(blank=True, null=True)
    # period_end = models.DateField(blank=True, null=True)
    # unit = models.CharField(max_length=50, choices=UNIT_CHOICES, default=KG)
    factory_id = models.ForeignKey(Factory, db_column='factory_id', related_name='%(class)s_factory_id',
                                   on_delete=models.PROTECT, blank=True, null=True)
    created_by = models.ForeignKey("user.User", blank=True, null=True, db_column='created_by',
                                   related_name='%(class)s_created_by', on_delete=models.PROTECT)
    created_date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    modified_by = models.ForeignKey("user.User", blank=True, null=True, db_column='modified_by',
                                    related_name='%(class)s_modified_by', on_delete=models.PROTECT)
    modified_date = models.DateTimeField(auto_now=True, blank=True, null=True)

class ProductHistory(models.Model):
    class Meta:
        db_table = "product_history"

    KG = 'KG'
    TON = 'TON'
    UNIT_CHOICES = (
        (KG, 'Kg'),
        (TON, 'TON'),
    )

    max_weight = models.DecimalField(max_digits=15, decimal_places=2, default=0, blank=True, null=True)
    min_weight = models.DecimalField(max_digits=15, decimal_places=2, default=0, blank=True, null=True)
    price = models.DecimalField(max_digits=15, decimal_places=2, default=0, blank=True, null=True)
    # unit = models.CharField(max_length=50, choices=UNIT_CHOICES, default=KG)
    order = models.IntegerField()
    product_id = models.ForeignKey(Product, db_column='product_id', related_name='%(class)s_product_id',
                                   on_delete=models.PROTECT, blank=True, null=True)