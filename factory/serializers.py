from rest_framework import serializers
from factory.models import Factory

class FactorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Factory
        fields = ('id', 'name', 'logo', 'status', 'address', 'phone', 'email', 'fax', 'city', 'latitude', 'longitude', 'website', 'created_by', 'modified_by', 'created_date', 'modified_date')
        extra_kwargs = {'id': {'read_only': True}}