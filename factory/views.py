from django.conf import settings
from django.utils.crypto import get_random_string
from rest_framework import viewsets, mixins
from rest_framework.permissions import AllowAny

from config.helper import MyResponse

from user import permissions
from factory.models import Factory
from factory.serializers import FactorySerializer

class FactoryViewSet(mixins.ListModelMixin,
                     mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet):

    queryset = Factory.objects.all()
    serializer_class = FactorySerializer

    def get_permissions(self):
        permission_classes = []
        if self.action == 'create':
            permission_classes = [permissions.AdminOnly]
        elif self.action == 'update' or self.action == 'partial_update':
            permission_classes = [permissions.ManagerOnly]
        elif self.action == 'retrieve':
            permission_classes = [permissions.AdminManagerOnly]
        elif self.action == 'list' or self.action == 'destroy':
            permission_classes = [permissions.AdminOnly]
        return [permission() for permission in permission_classes]
