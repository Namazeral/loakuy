from django.urls import path, include
from rest_framework import routers
from factory import views

router = routers.DefaultRouter()
router.register(r'', views.FactoryViewSet)

urlpatterns = [
    path('', include(router.urls)),
]