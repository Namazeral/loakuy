from django.db import models

from location.models import City, District

# Create your models here.

class Factory(models.Model):
    class Meta:
        db_table = "factory"

    ACTIVE = 'ACTIVE'
    NOT_ACTIVE = 'NOT_ACTIVE'
    NEW = "NEW"
    STATUS_CHOICES = (
        (ACTIVE, 'Active'),
        (NOT_ACTIVE, 'Not Active'),
        (NEW, 'New')
    )

    name = models.CharField(max_length=100, blank=True, null=True)
    logo = models.ImageField(upload_to="LoaKuy/image/factory/%Y/%m/%d", blank=True, null=True)
    status = models.CharField(max_length=50, choices=STATUS_CHOICES, default=ACTIVE)
    address = models.CharField(max_length=5000, blank=True, null=True)
    phone = models.CharField(max_length=100, blank=True, null=True)
    email = models.CharField(max_length=100, blank=True, null=True)
    fax = models.CharField(max_length=100, blank=True, null=True)
    city = models.ForeignKey(City, blank=True, null=True, db_column='city_id', related_name='%(class)s_city_id', on_delete=models.PROTECT)
    district = models.ForeignKey(District, blank=True, null=True, db_column='district_id', related_name='%(class)s_district_id', on_delete=models.PROTECT)
    latitude = models.FloatField(default=0.0)
    longitude = models.FloatField(default=0.0)
    website = models.CharField(max_length=50, choices=STATUS_CHOICES, default=ACTIVE)
    created_by = models.ForeignKey("user.User", blank=True, null=True, db_column='created_by',related_name='%(class)s_created_by', on_delete=models.PROTECT)
    created_date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    modified_by = models.ForeignKey("user.User", blank=True, null=True, db_column='modified_by',related_name='%(class)s_modified_by', on_delete=models.PROTECT)
    modified_date = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return '%s' % (self.id)